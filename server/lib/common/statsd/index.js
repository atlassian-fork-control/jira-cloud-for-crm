const logger = require('../logger').child({ module: 'statsd' })

module.exports = require('./statsd')(logger)
