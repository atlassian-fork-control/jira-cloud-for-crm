const responseTime = require('response-time')
const statsd = require('./index')

module.exports = responseTime((req, res, time) => {
  const tags = {
    method: req.method,
    response_code: res.statusCode,
    route_path: req.serviceName,
  }

  statsd.timing('http_request_in.response_time', time, tags)
  statsd.increment('http_request_in.count', tags)
})
