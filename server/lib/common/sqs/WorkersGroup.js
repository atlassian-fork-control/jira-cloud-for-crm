const Monitor = require('./Monitor')
const Worker = require('./Worker')

const scalingStrategy = {
  minWorkersCount: 1,
  maxWorkersCount: 1,
  maxMessagesCountDiff: 100000,
}

class WorkersGroup {
  constructor ({ logger, queue, client, handler, tracker }) {
    this.logger = logger
    this.handler = handler
    this.tracker = tracker

    this.client = client
    this.queue = queue

    this._queueAttributes = {}

    this._workers = []
  }

  _scale (newAttrs) {
    const messagesCountDiff =
      Number.parseInt(newAttrs.ApproximateNumberOfMessages, 10) -
      (this._queueAttributes.ApproximateNumberOfMessages || 0)

    const changeWorkerCountBy = Math.floor(messagesCountDiff / scalingStrategy.maxMessagesCountDiff)

    this._queueAttributes = newAttrs

    if (changeWorkerCountBy > 0) {
      for (let i = 0; i < changeWorkerCountBy; i++) {
        this.addWorker()
      }
    }

    if (changeWorkerCountBy < 0) {
      for (let i = 0; i > changeWorkerCountBy; i--) {
        this.removeWorker()
      }
    }
  }

  startMonitor () {
    const monitor = new Monitor({
      queue: this.queue,
      logger: this.logger,
      client: this.client,
      handler: (queueAttrs) => {
        this.tracker(queueAttrs)
        this._scale(queueAttrs)
      },
    })

    monitor.start()

    this._monitor = monitor
  }

  addWorker () {
    if (this._workers.length >= scalingStrategy.maxWorkersCount) {
      return
    }

    const worker = new Worker({
      queue: this.queue,
      logger: this.logger,
      client: this.client,
      handler: this.handler,
    })

    worker.start()

    this._workers.push(worker)
  }

  removeWorker () {
    if (this._workers.length <= scalingStrategy.minWorkersCount) {
      return
    }

    const worker = this._workers.shift()

    return worker.stop()
  }

  start () {
    this.startMonitor()

    for (let i = 0; i < scalingStrategy.minWorkersCount; i++) {
      this.addWorker()
    }
  }

  stop () {
    return Promise.all([
      this._monitor.stop(),
      ...this._workers.map(worker => worker.stop()),
    ])
  }
}

module.exports = WorkersGroup
