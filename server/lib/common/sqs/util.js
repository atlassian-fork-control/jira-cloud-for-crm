function handleError ({ error, msg, logger }) {
  if (error.code === 'AccessDenied') {
    // It suppresses the noise in Splunk caused by AWS everyday failover
    logger.warn({ err: error }, 'Access to AWS is denied')
  } else {
    logger.error({ err: error }, msg)
  }
}

module.exports = {
  handleError,
}
