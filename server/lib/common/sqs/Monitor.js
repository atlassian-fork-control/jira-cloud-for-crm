const { handleError } = require('./util')

class Monitor {
  constructor ({ logger, client, queue, handler }) {
    this.logger = logger
    this.client = client
    this.queue = queue
    this.handler = handler

    this._running = false
  }

  start () {
    const params = {
      AttributeNames: [
        'ApproximateNumberOfMessages',
        'ApproximateNumberOfMessagesDelayed',
        'ApproximateNumberOfMessagesNotVisible',
      ],
      QueueUrl: this.queue.url,
    }

    this._interval = setInterval(async () => {
      try {
        const data = await this.client.getQueueAttributes(params).promise()
        const attributes = data.Attributes

        await this.handler(attributes)
      } catch (error) /* istanbul ignore next */ {
        handleError({
          error,
          msg: 'Error while "getQueueAttributes"',
          logger: this.logger,
        })
      }
    }, 60 * 1000)
  }

  async stop () {
    clearInterval(this._interval)
  }
}

module.exports = Monitor
