const fetch = require('node-fetch')
const statsd = require('../statsd')

module.exports = serviceName => async (state, apiMethod = 'unknown') => {
  const startTime = Date.now()
  const response = await fetch(state.req.url, state.req)
  const totalTime = Date.now() - startTime

  const tags = {
    api_method: apiMethod,
    method: state.req.method || 'GET',
    response_code: response.status,
    external_service: serviceName,
  }

  statsd.timing(`http_request_out.response_time`, totalTime, tags)
  statsd.increment('http_request_out.count', tags)

  state.res = {
    headers: response.headers.raw(),
    status: response.status,
  }

  state.res.body = await response.text()

  const isJSON = (response.headers.get('content-type') || '').includes('application/json')

  if (isJSON && state.res.body) {
    state.res.body = JSON.parse(state.res.body)
  }

  if (!response.ok) {
    throw new Error(response.statusText)
  }

  return state
}
