const querystring = require('querystring')
const { get } = require('lodash')
const config = require('../config').get('crm.hubspot')
const client = require('./client')('hubspot')
const { delayPromised } = require('../../services/util')

class Hubspot {
  constructor (account) {
    this.account = account
  }

  getTokenInfo () {
    return this._fetch('getTokenInfo',
      { pathname: `/oauth/v1/access-tokens/${this.account.accessToken}` },
      { headers: { Authorization: null } })
  }

  getObjectFieldsMetadata (recordType) {
    const pluralMap = {
      Contact: 'contacts',
      Company: 'companies',
    }

    return this._fetch('getObjectFieldsMetadata', { pathname: `/properties/v1/${pluralMap[recordType]}/properties` })
  }

  async getContactById (contactId, properties) {
    const query = { properties }

    try {
      return await this._fetch('getContactById', { pathname: `/contacts/v1/contact/vid/${contactId}/profile`, query })
    } catch (error) {
      if (get(error, 'res.body.message') === 'contact does not exist') {
        return null
      }

      throw error
    }
  }

  async getContactByEmail (contactEmail) {
    try {
      return await this._fetch('getContactByEmail', { pathname: `/contacts/v1/contact/email/${contactEmail}/profile` })
    } catch (error) {
      if (get(error, 'res.body.message') === 'contact does not exist') {
        return null
      }

      throw error
    }
  }

  getContactsByCompany (companyId, properties) {
    const query = { properties, count: 3 }

    return this._fetch('getContactsByCompany', { pathname: `/companies/v2/companies/${companyId}/contacts`, query })
  }

  getContactsList (search) {
    const query = { q: search }

    return this._fetch('getContactsList', { pathname: '/contacts/v1/search/query', query })
  }

  getCompanyById (companyId, properties) {
    const query = { properties }

    return this._fetch('getCompanyById', { pathname: `/companies/v2/companies/${companyId}`, query })
  }

  getDealsByContact (contactId, properties) {
    const query = { properties: ['pipeline', ...properties], limit: 3 }

    return this._fetch('getDealsByContact', { pathname: `/deals/v1/deal/associated/contact/${contactId}/paged`, query })
  }

  getDealsByCompany (companyId, properties) {
    const query = { properties: ['pipeline', ...properties], limit: 3 }

    return this._fetch('getDealsByCompany', { pathname: `/deals/v1/deal/associated/company/${companyId}/paged`, query })
  }

  getPipelineById (pipelineId) {
    return this._fetch('getPipelineById', { pathname: `/deals/v1/pipelines/${pipelineId}` })
  }

  getNewToken () {
    const body = querystring.stringify({
      grant_type: 'refresh_token',
      client_id: config.clientId,
      client_secret: config.clientSecret,
      refresh_token: this.account.refreshToken,
    })

    const headers = {
      Authorization: null,
      'Content-Type': 'application/x-www-form-urlencoded',
    }

    return this._fetch(
      'getNewToken',
      { pathname: '/oauth/v1/token' },
      { method: 'POST', body, headers }
    )
  }

  async _fetch (
    methodName,
    { host, pathname, query },
    { method = 'GET', body, headers = {} } = {}
  ) {
    const url = new URL(pathname, host || 'https://api.hubapi.com')

    if (query) {
      url.search = querystring.stringify(query)
    }

    if (headers['Content-Type'] === undefined) {
      headers['Content-Type'] = 'application/json'
    }

    if (headers.Authorization === undefined) {
      headers.Authorization = `Bearer ${this.account.accessToken}`
    }

    if (body && headers['Content-Type'] === 'application/json') {
      body = JSON.stringify(body)
    }

    const state = {
      req: {
        method,
        headers,
        body,
        url: url.toString(),
      },
      res: {},
    }

    try {
      await client(state, methodName)

      return state.res.body
    } catch (error) {
      const response = get(state, 'res.body', {})

      const errorMessage = response.message || ''

      const tokenExpired = (
        state.res.status === 401 &&
        (errorMessage.includes('is expired!') || errorMessage.includes('is not valid for this request!'))
      ) || (
        // We get this when trying to use getTokenInfo with expired token
        state.res.status === 404 && errorMessage.includes('access token is expired or invalid')
      )

      if (tokenExpired) {
        const { access_token: accessToken } = await this.getNewToken()

        await this.account.update({ accessToken })

        if (methodName === 'getTokenInfo') {
          return this.getTokenInfo()
        }

        return this._fetch.apply(this, arguments) // eslint-disable-line
      }

      // Secondly rate limit is 10 requests per second
      if (
        response.errorType === 'RATE_LIMIT' &&
        response.policyName === 'SECONDLY'
      ) {
        await delayPromised(1000)

        return this._fetch.apply(this, arguments) // eslint-disable-line
      }

      const fields = {
        originError: error,
        source: 'hubspot',
      }

      if (['BAD_REFRESH_TOKEN', 'BAD_HUB'].includes(get(state, 'res.body.status'))) {
        fields.code = 'CRM_ACCOUNT_INACTIVE'
      }

      throw Object.assign(
        new Error('Hubspot API error'),
        state,
        fields
      )
    }
  }
}

module.exports = Hubspot
