const moment = require('moment')
const jwt = require('atlassian-jwt')
const { get } = require('lodash')
const { format } = require('url')
const logger = require('../logger').child({ module: 'jira' })

const config = require('../config')

const key = config.get('addon.key')
const serviceName = config.get('service.name')
const { environment, microsEnvironmentType } = config.get('service')

const client = require('./client')('jira')

class Jira {
  constructor (instance) {
    this.instance = instance
  }

  searchUsers (query) {
    return this._fetch('searchUsers', { pathname: '/rest/api/2/user/permission/search', query })
  }

  getUser (userKey) {
    if (userKey) {
      return this._fetch('getUser', { pathname: '/rest/api/2/user', query: { key: userKey } })
    }

    return this._fetch('getUser', { pathname: '/rest/api/2/myself' })
  }

  async getUserEmail (accountId) {
    if (!(environment === 'test' || microsEnvironmentType === 'prod')) {
      logger.info('Email API is only available on production')

      return ''
    }

    try {
      const { email } = await this._fetch('getUserEmail', {
        pathname: '/rest/api/3/user/email',
        query: { accountId },
      })

      return email
    } catch (error) {
      logger.warn({ err: error }, `Failed to get user's email`)

      return ''
    }
  }

  getAllProjects (query) {
    return this._fetch('getAllProjects', { pathname: '/rest/api/2/project', query })
  }

  getProject (id) {
    return this._fetch('getProject', { pathname: `/rest/api/2/project/${id}` })
  }

  getServerInfo () {
    return this._fetch('getServerInfo', { pathname: '/rest/api/2/serverInfo' })
  }

  getIssues (body) {
    return this._fetch(
      'getIssues',
      { pathname: `/rest/api/3/search` },
      { method: 'POST', body }
    )
  }

  async getIssue (id) {
    try {
      return await this._fetch('getIssue', { pathname: `/rest/api/2/issue/${id}` })
    } catch (error) {
      const errorMessages = get(error, 'response.body.errorMessages', [])

      if (errorMessages.includes('Issue Does Not Exist')) return

      throw error
    }
  }

  async getProjectAdminPermission (projectId) {
    try {
      const { permissions } = await this._fetch('getProjectAdminPermission', {
        pathname: '/rest/api/2/mypermissions',
        query: { projectId },
      })

      const haveAdministerProjectsPermission = permissions.ADMINISTER_PROJECTS.havePermission
      const haveProjectAdminPermission = permissions.PROJECT_ADMIN.havePermission

      return haveAdministerProjectsPermission || haveProjectAdminPermission
    } catch (error) {
      const errorMessages = get(error, 'res.body.errorMessages', [])

      if (errorMessages.includes(`Could not find project with id ${projectId}`)) return

      throw error
    }
  }

  setProjectProperty (projectId, data) {
    return this._fetch(
      'setProjectProperty',
      { pathname: `/rest/api/2/project/${projectId}/properties/${key}` },
      { method: 'PUT', body: data }
    )
  }

  deleteProjectProperty (projectId) {
    return this._fetch(
      'deleteProjectProperty',
      { pathname: `/rest/api/2/project/${projectId}/properties/${key}` },
      { method: 'DELETE' }
    )
  }

  getAddonProperty (id) {
    return this._fetch('getAddonProperty', { pathname: `/rest/atlassian-connect/1/addons/${key}/properties/${id}` })
  }

  setAddonProperty (id, data) {
    return this._fetch(
      'setAddonProperty',
      { pathname: `/rest/atlassian-connect/1/addons/${key}/properties/${id}` },
      { method: 'PUT', body: data }
    )
  }

  deleteAddonProperty (id) {
    return this._fetch(
      'deleteAddonProperty',
      { pathname: `/rest/atlassian-connect/1/addons/${key}/properties/${id}` },
      { method: 'DELETE' }
    )
  }

  installAddon () {
    const headers = {
      'Content-Type': 'application/json',
    }

    const body = {
      addonKey: serviceName,
    }

    return this._fetch(
      'installAddon',
      { pathname: '/rest/atlassian-connect/1/systemaddons/install' },
      { method: 'POST', headers, body }
    )
  }

  asUser (userToken) {
    this.userToken = userToken

    return this
  }

  async getUserToken (accountId) {
    const { instance } = this

    const authorizationServerUrl = instance.baseUrl.endsWith('.jira-dev.com')
      ? 'https://auth-prod-version.dev.public.atl-paas.net'
      : 'https://auth.prod.public.atl-paas.net'

    const jwtClaimPrefix = 'urn:atlassian:connect'
    const grantType = 'urn:ietf:params:oauth:grant-type:jwt-bearer'

    const now = moment().utc()

    const payload = {
      tnt: instance.baseUrl,
      sub: [jwtClaimPrefix, 'useraccountid', accountId].join(':'),
      iss: [jwtClaimPrefix, 'clientid', instance.oauthClientId].join(':'),
      iat: now.unix(),
      exp: now.add(1, 'minutes').unix(),
      aud: authorizationServerUrl,
    }

    const assertion = jwt.encode(payload, instance.sharedSecret)

    const url = {
      host: authorizationServerUrl,
      pathname: '/oauth2/token',
      query: {
        grant_type: grantType,
        assertion,
        scopes: 'READ',
      },
    }

    const body = await this._fetch('getUserToken', url, {
      method: 'POST',
      headers: {
        'Content-Type': 'application/x-www-form-urlencoded',
        Accept: 'application/json',
      },
    })

    return { token: body.access_token, expiresIn: body.expires_in }
  }

  _makeAuthHeader (method, url) {
    if (this.userToken) {
      return `Bearer ${this.userToken}`
    }

    if (!this.instance.sharedSecret) {
      return
    }

    const req = jwt.fromMethodAndUrl(method, url)

    const now = moment().utc()

    const payload = {
      qsh: jwt.createQueryStringHash(req),
      iss: key,
      iat: now.unix(),
      exp: now.add(1, 'minutes').unix(),
      aud: [this.instance.id],
    }

    return `JWT ${jwt.encode(payload, this.instance.sharedSecret, 'HS256')}`
  }

  async _fetch (
    methodName,
    { host, pathname, query },
    { method = 'GET', body, headers = {} } = {}
  ) {
    const url = format({
      host: host || this.instance.baseUrl,
      pathname,
      query,
    })

    if (headers['Content-Type'] === undefined) {
      headers['Content-Type'] = 'application/json'
    }

    if (headers.Authorization === undefined) {
      headers.Authorization = this._makeAuthHeader(method, url)
    }

    // This enforces GDPR changes
    headers['x-atlassian-force-account-id'] = true

    // strong check for undefined
    // cause body variable can be 'false' boolean value
    if (body && headers['Content-Type'] === 'application/json') {
      body = JSON.stringify(body)
    }

    const state = {
      req: {
        method,
        headers,
        body,
        url: url.toString(),
      },
      res: {},
    }

    try {
      await client(state, methodName)

      return state.res.body
    } catch (error) {
      const fields = {
        originError: error,
        source: 'jira',
      }

      throw Object.assign(
        new Error('Jira API error'),
        state,
        fields
      )
    }
  }
}

module.exports = Jira
