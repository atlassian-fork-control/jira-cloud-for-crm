const hash = require('object-hash')

module.exports = {
  allowedToCache (target, property) {
    const excludedProperties = ['getNewToken', 'getTokenInfo']

    if (excludedProperties.includes(property)) {
      return false
    }

    if (property.startsWith('get')) {
      return true
    }
  },
  getKey ({ target, property, params }) {
    return hash({
      accountId: target.account.id,
      property,
      params,
    })
  },
}
