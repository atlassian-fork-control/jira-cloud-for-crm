const { isFunction } = require('lodash')
const strategies = require('./strategies')
const { isExternalApiCallsCached } = require('../../config').get('service')

const ttl = 60 * 60 * 24 * 7 // 7 days

module.exports = (cache, logger) => {
  async function updateCache ({ key, target, property, params }) {
    try {
      const result = await target[property](...params)

      try {
        await cache.setAsync(key, JSON.stringify(result), ttl)
      } catch (error) {
        if (error.message.includes('The length of the value is greater than')) {
          logger.warn(
            { err: error, cacheOptions: { property, params } },
            'API response is too large'
          )
        } else {
          logger.error({ err: error }, 'Error while caching API response')
        }

        await cache.setAsync(key, null, 1) // more silent version of delAsync
      }

      return result
    } catch (error) {
      await cache.setAsync(key, null, 1) // more silent version of delAsync

      throw error
    }
  }

  return function getHandler (serviceName) {
    const strategy = strategies[serviceName.toLowerCase()]

    // See Proxy specification to get idea how this works
    function get (target, property) {
      // Network requests caching is disabled for the most of tests
      if (!isExternalApiCallsCached) {
        return target[property]
      }

      // Do not proxy properties
      if (!isFunction(target[property])) {
        return target[property]
      }

      // Do not proxy mutating methods
      if (!strategy.allowedToCache(target, property)) {
        return target[property]
      }

      // If property value is method, and cache strategy allowed to cache it -
      // return wrapper function with cache implementation
      return async (...params) => {
        const key = `${serviceName}:${strategy.getKey({
          target,
          params,
          property,
        })}`

        // Start updating cache immediately
        const promise = updateCache({ key, target, property, params })

        const cachedResult = await cache.getAsync(key)

        // logger.info({ serviceName, property }, cachedResult ? 'Cache hit' : 'Cache miss')

        if (cachedResult) {
          // Detached error handler
          promise.catch(error => logger.error({ err: error }, 'Detached network error'))

          return JSON.parse(cachedResult)
        }

        return promise
      }
    }

    return { get }
  }
}
