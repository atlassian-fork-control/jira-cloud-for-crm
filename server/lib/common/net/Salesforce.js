const querystring = require('querystring')
const { get } = require('lodash')
const knexSoql = require('knex-soql')
const knex = require('knex')({ client: knexSoql })

const config = require('../config').get('crm.salesforce')
const gas = require('../gas')

const client = require('./client')('salesforce')

class Salesforce {
  constructor (account) {
    if (!account) {
      throw new Error('No account')
    }

    this.account = account
  }

  getMyself () {
    return this._fetch('getMyself', {
      host: 'https://login.salesforce.com',
      pathname: '/services/oauth2/userinfo',
    })
  }

  getObjectFieldsMetadata (recordType) {
    return this._fetch('getObjectFieldsMetadata', { pathname: `/services/data/v40.0/sobjects/${recordType}/describe` })
  }

  getOpportunitiesByAccount (accountId, fields) {
    const subquery = knex('Account.Opportunities')
      .select(['Opportunity.Id', 'Opportunity.Name', ...fields])
      .orderBy('LastModifiedDate', 'desc')
      .limit(3)

    const query = knex('Account')
      .select([subquery])
      .where({ 'Account.Id': accountId })

    return this._soql('getOpportunitiesByAccount', query)
  }

  getOpportunitiesByContact (contactId, fields) {
    const newFields = fields.map(field => `OpportunityContactRole.${field}`)

    const subquery = knex('Contact.OpportunityContactRoles')
      .select(['OpportunityContactRole.Opportunity.Id', 'OpportunityContactRole.Opportunity.Name', ...newFields])
      .orderBy('LastModifiedDate', 'desc')
      .limit(3)

    const query = knex('Contact')
      .select([subquery])
      .where({ Id: contactId })

    return this._soql('getOpportunitiesByContact', query)
  }

  getContactsByAccount (accountId, fields) {
    const subquery = knex('Account.Contacts')
      .select(['Contact.Id', 'Contact.Name', ...fields])
      .orderBy('LastModifiedDate', 'desc')
      .limit(3)

    const query = knex('Account')
      .select([subquery])
      .where({ 'Account.Id': accountId })

    return this._soql('getContactsByAccount', query)
  }

  getCasesByContact (contactId, fields) {
    const subquery = knex('Contact.Cases')
      .select(['Case.Id', ...fields])
      .orderBy('LastModifiedDate', 'desc')
      .limit(3)

    const query = knex('Contact')
      .select([subquery])
      .where({ Id: contactId })

    return this._soql('getCasesByContact', query)
  }

  getCasesByAccount (accountId, fields) {
    const subquery = knex('Account.Cases')
      .select(['Case.Id', ...fields])
      .orderBy('LastModifiedDate', 'desc')
      .limit(3)

    const query = knex('Account')
      .select([subquery])
      .where({ 'Account.Id': accountId })

    return this._soql('getCasesByAccount', query)
  }

  getAssetsByContact (contactId, fields) {
    const product = ['Product2.Id', 'Product2.Name']

    const subquery = knex('Contact.Assets')
      .select(['Asset.Id', 'Asset.Name', ...product, ...fields])
      .orderBy('PurchaseDate', 'desc')
      .limit(3)

    const query = knex('Contact')
      .select([subquery])
      .where({ Id: contactId })

    return this._soql('getAssetsByContact', query)
  }

  getAssetsByAccount (accountId, fields) {
    const product = ['Product2.Id', 'Product2.Name']

    const subquery = knex('Account.Assets')
      .select(['Asset.Id', 'Asset.Name', ...product, ...fields])
      .orderBy('PurchaseDate', 'desc')
      .limit(3)

    const query = knex('Account')
      .select([subquery])
      .where({ Id: accountId })

    return this._soql('getAssetsByAccount', query)
  }

  getEntitlementsByAccount (accountId, fields) {
    const asset = ['Asset.Id', 'Asset.Name']

    const subquery = knex('Account.Entitlements')
      .select(['Entitlement.Id', 'Entitlement.Name', ...asset, ...fields])
      .orderBy('LastModifiedDate', 'desc')
      .limit(3)

    const query = knex('Account')
      .select([subquery])
      .where({ Id: accountId })

    return this._soql('getEntitlementsByAccount', query)
  }

  getAccountList (search) {
    const query = knex('Account')
      .select(['Id', 'Name', 'PhotoUrl'])
      .where('Name', 'like', `%${search}%`)

    return this._soql('getAccountList', query)
  }

  getAccounts (where, fields = []) {
    const query = knex('Account')
      .select(['Id', 'Name', ...fields])
      .where(where)

    return this._soql('getAccounts', query)
  }

  getContactList (search) {
    const pattern = `${search}%`

    const query = knex('Contact')
      .select(['Id', 'Name', 'Email', 'PhotoUrl', 'Account.Name'])
      .where('FirstName', 'like', pattern)
      .orWhere('LastName', 'like', pattern)
      .orWhere('Email', 'like', pattern)

    return this._soql('getContactList', query)
  }

  getContacts (where, fields = ['Email']) {
    const query = knex('Contact')
      .select(['Id', 'Name', ...fields])
      .where(where)

    return this._soql('getContacts', query)
  }

  async getLeadList (search) {
    const pattern = `${search}%`

    const query = knex('Lead')
      .select(['Id', 'Name', 'Email', 'PhotoUrl'])
      .where('FirstName', 'like', pattern)
      .orWhere('LastName', 'like', pattern)
      .orWhere('Email', 'like', pattern)

    try {
      return await this._soql('getLeadList', query)
    } catch (error) {
      // TODO: Remove it later. Just want to see how often it happens
      gas.send({
        name: 'lead_unavailable_on_customer_list',
        cloudId: '-',
        user: '-',
      })

      return { records: [] }
    }
  }

  getLeads (where, fields = ['Email']) {
    const query = knex('Lead')
      .select(['Id, Name', ...fields])
      .where(where)

    return this._soql('getLeads', query)
  }

  getNewToken () {
    const body = querystring.stringify({
      grant_type: 'refresh_token',
      client_id: config.clientId,
      client_secret: config.clientSecret,
      refresh_token: this.account.refreshToken,
      format: 'json',
    })

    const headers = {
      Authorization: null,
      'Content-Type': 'application/x-www-form-urlencoded',
    }

    return this._fetch(
      'getNewToken',
      { host: 'https://login.salesforce.com', pathname: '/services/oauth2/token' },
      { method: 'POST', body, headers }
    )
  }

  _soql (methodName, query) {
    const q = typeof query === 'object' ? query.toString() : query

    return this._fetch(methodName, {
      pathname: '/services/data/v40.0/query',
      query: { q },
    })
  }

  _checkIfTokenExpired (responseBody) {
    if (responseBody === 'Bad_OAuth_Token') {
      return true
    }

    return Array.isArray(responseBody) && responseBody.some(
      error => error.errorCode === 'INVALID_SESSION_ID' &&
      error.message === 'Session expired or invalid'
    )
  }

  async _fetch (
    methodName,
    { host, pathname, query },
    { method = 'GET', body, headers = {} } = {}
  ) {
    const url = new URL(pathname, host || this.account.baseUrl)

    if (query) {
      url.search = querystring.stringify(query)
    }

    if (headers['Content-Type'] === undefined) {
      headers['Content-Type'] = 'application/json'
    }

    if (headers.Authorization === undefined) {
      headers.Authorization = `Bearer ${this.account.accessToken}`
    }

    if (body && headers['Content-Type'] === 'application/json') {
      body = JSON.stringify(body)
    }

    const state = {
      req: {
        method,
        headers,
        body,
        url: url.toString(),
      },
      res: {},
    }

    try {
      await client(state, methodName)

      return state.res.body
    } catch (error) {
      const responseBody = state.res.body

      const tokenExpired = this._checkIfTokenExpired(responseBody)

      if (tokenExpired) {
        const { access_token: accessToken } = await this.getNewToken()

        await this.account.update({ accessToken })

        return this._fetch.apply(this, arguments) // eslint-disable-line
      }

      const fields = {
        originError: error,
        source: 'salesforce',
      }

      const errorsList = ['inactive user', 'expired access/refresh token', 'inactive organization']

      // Salesforce account is deactivated, we can no longer use its credentials
      if (
        get(responseBody, 'error') === 'invalid_grant' &&
        errorsList.includes(get(responseBody, 'error_description'))
      ) {
        fields.code = 'INVALID_CLIENT'
      }

      throw Object.assign(
        new Error('Salesforce API error'),
        state,
        fields
      )
    }
  }
}

module.exports = Salesforce
