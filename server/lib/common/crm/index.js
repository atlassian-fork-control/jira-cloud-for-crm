module.exports = (crm) => {
  const className = crm.charAt(0).toUpperCase() + crm.substring(1).toLowerCase()
  return require(`./${className}/${className}`) // eslint-disable-line
}
