module.exports = class CRMError extends Error {
  constructor (message, { code, data }) {
    super(message)

    this.message = message
    this.code = code
    this.data = data
  }
}
