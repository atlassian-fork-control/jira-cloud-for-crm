const crypto = require('crypto')
const Route = require('../Route')

const {
  crm: { hubspot: { clientSecret } },
} = require('../../common/config').get()

module.exports = class extends Route {
  async check (req, res, next) {
    // https://developers.hubspot.com/docs/methods/crm-extensions/crm-extensions-overview#request-signatures
    const signature = req.headers['x-hubspot-signature']

    const verifier = `${clientSecret}${req.method}https://${req.headers.host}${req.originalUrl}`
    const verifierHash = crypto.createHash('sha256').update(verifier).digest('hex')

    if (!signature || !crypto.timingSafeEqual(Buffer.from(signature), Buffer.from(verifierHash))) {
      return res.status(401).send({
        error: { code: 'UNAUTHORIZED' },
      })
    }

    const account = await this.model.Account.findOne({
      where: {
        type: 'hubspot',
        externalId: req.query.portalId,
      },
      include: [{
        model: this.model.Instance,
        required: true,
      }],
    })

    if (!account) {
      return res.send({
        results: [{
          objectId: null,
          title: 'Connect Jira project to HubSpot to see issues here',
          link: 'https://marketplace.atlassian.com/plugins/jira-crm-integration/cloud/overview',
        }],
      })
    }

    req.context = account
    req.logger = req.logger.child({ context: req.context })

    next()
  }
}
