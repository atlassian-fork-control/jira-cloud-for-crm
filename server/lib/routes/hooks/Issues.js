const Route = require('../Route')

module.exports = class extends Route {
  create (req, res) {
    this.initService('hooks.issues.Create', req.body).run(req, res)
  }

  update (req, res) {
    this.initService('hooks.issues.Update', req.body).run(req, res)
  }

  delete (req, res) {
    this.initService('hooks.issues.Delete', req.body).run(req, res)
  }
}
