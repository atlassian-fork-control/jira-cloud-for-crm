const requireDirectory = require('require-directory')

const routes = requireDirectory(module)
const { get } = require('lodash')

const buildRoute = ({ model, services }) => (fullPath, options) => {
  const parts = fullPath.split('.')
  const methodName = parts.pop()
  const Class = get(routes, parts)

  const object = new Class({
    fullPath,
    model,
    services,
  }, options)

  return object[methodName].bind(object)
}

module.exports = { buildRoute }
