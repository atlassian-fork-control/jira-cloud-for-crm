const { QueryTypes } = require('sequelize')
const Route = require('./Route')

module.exports = class extends Route {
  async get (req, res) {
    try {
      await Promise.all([
        this.model.sequelize.query('SELECT 1+1 AS result', { type: QueryTypes.SELECT }),
        this.model.sequelize.query('SELECT 1+1 AS result', { type: QueryTypes.SELECT, useMaster: true }),
      ])

      res.json({ ok: true })
    } catch (error) {
      req.logger.fatal({ err: error }, 'Failed healthcheck!')

      res.status(500).json({ ok: false })
    }
  }
}
