const uuid = require('uuid/v4')
const crypto = require('crypto')
const { parse: parseCookies } = require('cookie')
const Session = require('./Session')

module.exports = class extends Session {
  extractToken (req) {
    const token = req.headers.authorization || req.query['addon-token'] || req.query.state || ''

    return token.replace(/^JWT /, '')
  }

  extractContext (data = {}) {
    return {
      jiraAccountId: data.sub,
    }
  }

  async initCsrf (req, res, next) {
    const ttl = 2 * 3600 // 2 hours
    const cacheKey = `csrf:${req.context.jiraAccountId}`
    const csrfToken = uuid()

    // store CSRF token in cache associated with user's account id
    // as well as in user's browser cookies so likely
    // attacker won't have access to CSRF in cookies and JWT token at the same time

    await this.model.sequelize.cache.setAsync(cacheKey, csrfToken, ttl)

    res.cookie('csrf', csrfToken, {
      maxAge: ttl * 1000, // 2 hours in ms
      secure: true,
      httpOnly: true,
    })

    next()
  }

  async checkCsrf (req, res, next) {
    const cacheKey = `csrf:${req.context.jiraAccountId}`
    const cachedCsrf = await this.model.sequelize.cache.getAsync(cacheKey)

    const cookies = parseCookies(req.headers.cookie || '')

    const timingSafeEqual = (a = '', b = '') => (
      a.length === b.length &&
      crypto.timingSafeEqual(Buffer.from(a), Buffer.from(b))
    )

    if (!cachedCsrf || !timingSafeEqual(cachedCsrf, cookies.csrf)) {
      const message = 'Invalid or missing CSRF token'

      req.logger.warn(message)

      return res.status(403).json({
        error: {
          message,
          code: 'INVALID_CSRF',
        },
      })
    }

    next()
  }
}
