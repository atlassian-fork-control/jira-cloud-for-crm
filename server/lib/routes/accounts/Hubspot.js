const url = require('url')
const passport = require('passport')
const OAuth2Strategy = require('passport-oauth2')
const Route = require('../Route')
const { addon, crm } = require('../../common/config').get('')
const initNet = require('../../common/net')

const options = {
  authorizationURL: 'https://app.hubspot.com/oauth/authorize',
  tokenURL: 'https://api.hubapi.com/oauth/v1/token',
  scope: 'contacts',
  clientID: crm.hubspot.clientId,
  clientSecret: crm.hubspot.clientSecret,
  callbackURL: new URL('/api/accounts/create/hubspot/callback', addon.localBaseUrl).href,
}

const strategy = new OAuth2Strategy(
  options,
  (accessToken, refreshToken, profile, done) => done(null, ({
    tokens: {
      accessToken,
      refreshToken,
    },
  }))
)

passport.use('hubspot-oauth', strategy)

module.exports = class extends Route {
  authenticate (req, res, next) {
    const authenticate = passport.authenticate('hubspot-oauth', {
      state: req.query['addon-token'],
      session: false,
    })

    authenticate(req, res, (error) => {
      if (error) return this._authErrorMiddleware(error, req, res)
      next()
    })
  }

  async callback (req, res) {
    try {
      const { accessToken } = req.user.tokens

      const hubspot = initNet(this.model.sequelize.cache, req.logger)(
        'Hubspot',
        this.model.Account.build({ accessToken })
      )

      const tokenInfo = await hubspot.getTokenInfo()

      const account = await this.initService('accounts.Create', {
        type: 'hubspot',
        tokens: req.user.tokens,
        crmUserId: tokenInfo.user_id,
        externalId: tokenInfo.hub_id,
        baseUrl: `https://${tokenInfo.hub_domain}`,
      }).run(req)

      const pageUrl = url.format({
        host: addon.localBaseUrl,
        pathname: '/account_connected',
        query: { accountId: account.id },
      })

      res.redirect(pageUrl)
    } catch (error) {
      this._authErrorMiddleware(error, req, res)
    }
  }

  _authErrorMiddleware (error, req, res) {
    const { code, message } = error

    if (code && message) {
      req.logger.error({ err: error }, message)
      res.redirect(`/error?code=${code}&message=${message}&error_id=${req.id}`)
    } else {
      req.logger.error({ err: error }, 'HubSpot authentication error.')
      res.redirect(`/error?error_id=${req.id}`)
    }
  }
}
