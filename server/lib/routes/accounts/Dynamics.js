const url = require('url')
const { get } = require('lodash')
const passport = require('passport')
const OAuth2Strategy = require('passport-oauth2')
const jwt = require('atlassian-jwt')
const Route = require('../Route')
const { addon, crm } = require('../../common/config').get('')
const initNet = require('../../common/net')

class DynamicsOAuth2Strategy extends OAuth2Strategy {
  authorizationParams (options) {
    return {
      // azure AD requires user's Dynamics CRM Domain as `resource`
      // parameter in query of authorizationURL
      // https://docs.microsoft.com/en-us/azure/active-directory/develop/active-directory-protocols-oauth-code
      resource: options.resource,
    }
  }
}

const options = {
  authorizationURL: 'https://login.microsoftonline.com/common/oauth2/authorize',
  tokenURL: 'https://login.microsoftonline.com/common/oauth2/token',
  clientID: crm.dynamics.clientId,
  clientSecret: crm.dynamics.clientSecret,
  callbackURL: new URL('/api/accounts/create/dynamics/callback', addon.localBaseUrl).href,
}

const strategy = new DynamicsOAuth2Strategy(
  options,
  (accessToken, refreshToken, profile, done) => done(null, ({
    tokens: {
      accessToken,
      refreshToken,
    },
    profile: jwt.decode(accessToken, null, true), // retrieve user's info from access token payload
  }))
)

passport.use('dynamics-oauth', strategy)

module.exports = class extends Route {
  authenticate (req, res, next) {
    const resource = this._parseDynamicsURL(req.query.domain)

    const authenticate = passport.authenticate('dynamics-oauth', {
      state: req.query['addon-token'],
      session: false,
      resource,
    })

    authenticate(req, res, (error) => {
      if (error) return this._authErrorMiddleware(error, req, res)
      next()
    })
  }

  async callback (req, res) {
    try {
      const [baseUrl] = req.user.profile.aud
      const { accessToken } = req.user.tokens

      const dynamics = initNet(this.model.sequelize.cache, req.logger)(
        'Dynamics',
        this.model.Account.build({ baseUrl, accessToken })
      )

      const accountInfo = await dynamics.whoAmI()

      const account = await this.initService('accounts.Create', {
        type: 'dynamics',
        tokens: req.user.tokens,
        crmUserId: accountInfo.UserId,
        externalId: accountInfo.OrganizationId,
        baseUrl,
      }).run(req)

      const pageUrl = url.format({
        host: addon.localBaseUrl,
        pathname: '/account_connected',
        query: { accountId: account.id },
      })

      res.redirect(pageUrl)
    } catch (error) {
      this._authErrorMiddleware(error, req, res)
    }
  }

  _authErrorMiddleware (error, req, res) {
    let { code, message } = error

    if (get(error, res.body, '').includes('Unauthorized')) {
      code = 'UNAUTHORIZED'
      message = `You don't have access to this CRM account.`
    }

    if (code && message) {
      req.logger.error({ err: error }, message)
      res.redirect(`/error?code=${code}&message=${message}&error_id=${req.id}`)
    } else {
      req.logger.error({ err: error }, 'Dynamics authentication error.')
      res.redirect(`/error?error_id=${req.id}`)
    }
  }

  _parseDynamicsURL (string) {
    if (!string) return
    const link = /^(http|https):\/\//.test(string) ? string : `https://${string}`
    const { hostname } = new url.URL(link)
    const domain = hostname || ''

    return `https://${domain}`
  }
}
