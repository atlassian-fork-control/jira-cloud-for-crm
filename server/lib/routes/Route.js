const { get } = require('lodash')

const ServiceError = require('../services/Error')
const statsd = require('../common/statsd')

module.exports = class Route {
  constructor ({ services, model, fullPath }) {
    this.services = services
    this.model = model
    this.fullPath = fullPath
  }

  initService (name, params) {
    // Return callback here only to separate "name" and "params" from "req" and "res" arguments
    // so, we can call initService(name, params).run(req, res) instead of
    // initService(name, params, req, res)

    const run = async (req, res) => {
      req.serviceName = [
        req.serviceName,
        `r_${this.fullPath}`,
        `s_${name}`,
      ].filter(Boolean).join('_')

      try {
        const data = await this.runService({
          name,
          params,
          logger: req.logger,
          context: req.context,
        })

        if (!res) {
          return data
        }

        this.renderSuccess(data, req, res)
      } catch (error) {
        if (!res) {
          throw error
        }

        this.renderError(error, req, res)
      }
    }

    return { run }
  }

  async runService ({ name, params, logger, context }) {
    const Service = get(this.services, name)

    if (!Service) {
      throw new Error(`Can't find service by name: ${name}`)
    }

    const service = new Service({
      model: this.model,
      logger,
      context,
    })

    try {
      const data = await service.run(params)

      statsd.increment(name, { status: 'success' })

      return data
    } catch (error) {
      statsd.increment(name, { status: 'error', code: error.code })

      throw error
    }
  }

  renderSuccess (data, req, res) {
    const status = {
      PUT: 201,
      POST: 201,
      PATCH: 200,
      DELETE: 200,
      GET: 200,
    }[req.method]

    res.status(status || 200)

    return res.json({
      ok: true,
      data,
    })
  }

  renderError (error, req, res) {
    if (error instanceof ServiceError) {
      return res.status(400).json({ error: error.toJSON() })
    }

    req.logger.error({ err: error }, 'Unexpected error')

    res.status(500).json({
      error: {
        message: 'Something went wrong! Please try again later.',
        code: 'UNEXPECTED_ERROR',
        reqId: req.id,
        source: error.source,
      },
    })
  }
}
