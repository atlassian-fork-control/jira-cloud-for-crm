const express = require('express')
const compression = require('compression')
const helmet = require('helmet')
const bodyParser = require('body-parser')
const fallback = require('express-history-api-fallback')
const serveStatic = require('serve-static')

const allowedDomainsAddon = require('../common/config').get('addon.allowedDomains')
const allowedDomainsSalesforce = require('../common/config').get('crm.salesforce.allowedDomains')
const statsd = require('../common/statsd/middleware')
const { buildRoute } = require('./util')
const services = require('../services')

module.exports = ({ model }) => {
  const route = buildRoute({ model, services })

  const allowedDomains = [
    ...allowedDomainsAddon,
    ...allowedDomainsSalesforce,
  ].map(domain => `*${domain}`)

  const app = express()
    .use(compression())
    .use(helmet({
      frameguard: false,
      contentSecurityPolicy: {
        directives: { frameAncestors: allowedDomains },
      },
      hsts: {
        maxAge: 31536000, // 1 year
      },
    }))
    .use(bodyParser.raw({ limit: '10mb' }))
    .use(bodyParser.urlencoded({ limit: '10mb', extended: false }))
    .use(bodyParser.json({ limit: '10mb' }))
    .use(route('Middlewares.logger'))

  app.use('/api', express.Router()
    // Middlewares
    .use(statsd)
    .use(helmet.noCache())

    // Healthcheck
    .get('/healthcheck', route('Healthcheck.get'))

    // Version
    .get('/version', route('Version.get'))

    // Addon
    .get('/addon/descriptor', route('addon.Descriptor.get'))

    // Instances
    .post('/instances/create', route('sessions.Jira.check', { skipNoInstance: true, skipNoToken: true }), route('Instances.create'))
    .post('/instances/delete', route('sessions.Jira.check'), route('Instances.delete'))

    // Projects
    .post('/jira/projects/delete', route('sessions.Jira.check'), route('hooks.Projects.delete'))

    // Issues Hooks
    .post('/jira/issues/create', route('sessions.Jira.check'), route('hooks.Issues.create'))
    .post('/jira/issues/update', route('sessions.Jira.check'), route('hooks.Issues.update'))
    .post('/jira/issues/delete', route('sessions.Jira.check'), route('hooks.Issues.delete'))

    // Salesforce auth
    .get('/accounts/create/salesforce', route('sessions.Jira.check'), route('sessions.Jira.initCsrf'), route('accounts.Salesforce.authenticate'))
    .get('/accounts/create/salesforce/callback', route('sessions.Jira.check'), route('sessions.Jira.checkCsrf'), route('accounts.Salesforce.authenticate'), route('accounts.Salesforce.callback'))

    // MS Dynamics auth
    .get('/accounts/create/dynamics', route('sessions.Jira.check'), route('sessions.Jira.initCsrf'), route('accounts.Dynamics.authenticate'))
    .get('/accounts/create/dynamics/callback', route('sessions.Jira.check'), route('sessions.Jira.checkCsrf'), route('accounts.Dynamics.authenticate'), route('accounts.Dynamics.callback'))

    // Hubspot auth
    .get('/accounts/create/hubspot', route('sessions.Jira.check'), route('sessions.Jira.initCsrf'), route('accounts.Hubspot.authenticate'))
    .get('/accounts/create/Hubspot/callback', route('sessions.Jira.check'), route('sessions.Jira.checkCsrf'), route('accounts.Hubspot.authenticate'), route('accounts.Hubspot.callback'))

    // Salesforce interface pages
    .get('/salesforce', route('salesforce.Session.notApprovedOauth'))
    .post('/salesforce', route('salesforce.Session.init'))
    .get('/salesforce/issues', route('salesforce.Session.check'), route('salesforce.Issues.list'))

    // Hubspot integration
    .get('/hubspot/issues', route('hubspot.Session.check'), route('hubspot.Issues.list'))

    // Accounts
    .get('/accounts', route('sessions.Jira.check'), route('Accounts.show'))

    // Connections
    .post('/connections', route('sessions.Jira.check'), route('Connections.create'))
    .put('/connections/:id', route('sessions.Jira.check'), route('Connections.update'))
    .delete('/connections/:id', route('sessions.Jira.check'), route('Connections.delete'))
    .post('/connections/:id/sync', route('sessions.Jira.check'), route('Connections.sync'))

    // Customers
    .get('/customers', route('sessions.Jira.check'), route('Customers.list'))
    .get('/customers/projects/:projectId/issues/:issueId', route('sessions.Jira.check'), route('Customers.show'))

    // Matches
    .post('/matches', route('sessions.Jira.check'), route('Matches.create'))
    .delete('/matches', route('sessions.Jira.check'), route('Matches.delete'))

    // Admin
    .use('/admin', express.Router()
      .use(route('admin.Sessions.check'))
      .use('/instances', express.Router()
        .get('/', route('admin.Instances.list'))
        .post('/', route('admin.Instances.create'))
        .get('/:id', route('admin.Instances.show'))
        .delete('/:id', route('admin.Instances.delete'))
        .post('/:id/restore', route('admin.Instances.restore')))
      .use('/accounts', express.Router()
        .get('/', route('admin.Accounts.list'))
        .get('/:id', route('admin.Accounts.show'))
        .delete('/:id', route('admin.Accounts.delete'))
        .post('/:id/restore', route('admin.Accounts.restore')))))
    .use(serveStatic('../client/static', {
      maxAge: '365d',
    }))
    .use(serveStatic('../client/static/dist', {
      maxAge: '365d',
      extensions: ['html'],
    }))
    .use(fallback('index.html', { root: '../client/static/dist' }))
    .use(route('Middlewares.errorHandler'))

  return app
}
