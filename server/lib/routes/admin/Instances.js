const Route = require('../Route')

module.exports = class extends Route {
  list (req, res) {
    const parseSilent = (string) => {
      try {
        return JSON.parse(string)
      } catch (error) {
        //
      }
    }

    const order = parseSilent(req.query.order)
    const where = parseSilent(req.query.where)

    this.initService('admin.instances.List', Object.assign(
      {},
      req.query,
      { order, where }
    ))
      .run(req, res)
  }

  show (req, res) {
    this.initService('admin.instances.Show', {
      id: req.params.id,
    })
      .run(req, res)
  }

  delete (req, res) {
    this.initService('admin.instances.Delete', {
      id: req.params.id,
      force: req.body.force,
    })
      .run(req, res)
  }

  restore (req, res) {
    this.initService('admin.instances.Restore', {
      id: req.params.id,
    })
      .run(req, res)
  }

  create (req, res) {
    this.initService('admin.instances.Create', {
      instances: req.body.instances,
    })
      .run(req, res)
  }
}
