const Route = require('../Route')

module.exports = class extends Route {
  list (req, res) {
    this.initService('issues.List', {
      recordId: req.query.recordId,
      recordType: req.query.recordType,
    })
      .run(req, res)
  }
}
