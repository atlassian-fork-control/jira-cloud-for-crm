/* eslint-disable global-require, import/no-unresolved */

const appInfo = {
  nodeVersion: process.version,
  packageName: require('../../package.json').name,
}

try {
  appInfo.gitStuff = require('../../../build-output/git-version.json')
} catch (error) {
  appInfo.gitStuff = 'NOT_FOUND'
}

try {
  appInfo.version = require('../../../build-output/release-version.json').version
} catch (error) {
  appInfo.version = 'NOT_FOUND'
}

const Route = require('./Route')

module.exports = class Version extends Route {
  get (req, res) {
    this.renderSuccess(appInfo, req, res)
  }
}
