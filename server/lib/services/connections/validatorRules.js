const schema = { list_of_objects: {
  id: ['required', 'string'],
  label: ['required', 'string'],
  fields: ['required', { list_of_objects: {
    id: ['required', 'string'],
    label: ['required', 'string'],
    type: ['required', 'string'],
  } }],
  objects: ['required', { list_of_objects: {
    id: ['required', 'string'],
    label: ['required', 'string'],
    fields: ['required', { list_of_objects: {
      id: ['required', 'string'],
      label: ['required', 'string'],
      type: ['required', 'string'],
    } }],
  } }],
} }

module.exports = { schema }
