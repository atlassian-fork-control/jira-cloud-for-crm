const { get } = require('lodash')
const { Op } = require('sequelize')
const Service = require('../Service')
const StreamHub = require('../../common/net/StreamHub')
const statsd = require('../../common/statsd')

const { statuses } = StreamHub

module.exports = class extends Service {
  validate (params) {
    return params
  }

  async execute (message) {
    // We don't need to process test events
    const isSampled = get(message, 'traceContext.sampled')

    if (isSampled) {
      return
    }

    this.logger.info({ data: { message } }, 'rtbf.incoming-event')
    statsd.increment('streamhub_event_in.count')

    const {
      userId: accountId,
      correlationId,
    } = get(message, 'payload', {})

    if (!accountId) {
      statsd.increment('streamhub_event_out.count', { status: 'MALFORMED_EVENT' })

      return this.logger.warn('rtbf.no-account-id')
    }

    const params = { accountId, correlationId }
    const streamHub = new StreamHub()

    try {
      const accounts = await this.model.Account.findAll({
        where: { jiraAccountId: accountId },
      })

      if (!accounts.length) {
        const event = await streamHub.acknowledgeEvent({
          ...params,
          status: statuses.notFound,
        })

        this.logger.info({ data: { event, ...params } }, 'rtbf.status.not_found')
        statsd.increment('streamhub_event_out.count', { status: statuses.notFound })

        return
      }

      await this.model.Account.update(
        {
          accessToken: null,
          refreshToken: null,
          tokenCreatedAt: null,
        },
        {
          where: {
            id: { [Op.in]: accounts.map(account => account.id) },
          },
        },
      )

      const event = await streamHub.acknowledgeEvent({
        ...params,
        status: statuses.success,
      })

      this.logger.info({ data: { event, ...params } }, 'rtbf.status.success')
      statsd.increment('streamhub_event_out.count', { status: statuses.success })
    } catch (error) {
      this.logger.error({ err: error }, 'Error while processing RTBF event from StreamHub')

      const event = await streamHub.acknowledgeEvent({
        ...params,
        status: statuses.failure,
        errorMessage: error.message,
      })

      this.logger.info({ data: { event, ...params } }, 'rtbf.status.failure')
      statsd.increment('streamhub_event_out.count', { status: statuses.failure })
    }
  }
}
