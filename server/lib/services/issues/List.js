const { flatten } = require('lodash')
const getCRMClass = require('../../common/crm')

const Service = require('../Service')

module.exports = class extends Service {
  validate (params) {
    const rules = {
      recordType: ['required', 'string'],
      recordId: ['required', 'string'],
    }

    return this.validator(params, rules)
  }

  async execute ({ recordId, recordType }) {
    const accounts = await this.model.Account.findAll({
      where: {
        externalId: this.context('externalId'),
      },
      include: [{
        model: this.model.Instance,
        required: true,
      }, {
        model: this.model.Connection,
        required: true,
        include: {
          model: this.model.Match,
          where: { recordType, recordId },
          required: false,
        },
      }],
    })

    const issues = await Promise.all(accounts.map(async (account) => {
      const CRMClass = getCRMClass(account.type)
      const crm = new CRMClass(this.api.bind(this), account)
      const issuesByAccount = await this._getAccountIssues(account)

      return issuesByAccount.map(crm.formatJiraIssue)
    }))

    return flatten(issues)
  }

  async _getAccountIssues (account) {
    const issuesIds = flatten(
      account.Connections
        .map(connection => connection.Matches.map(match => match.issueId))
    )

    if (!issuesIds.length) return []

    const jql = `id in (${issuesIds.join(',')}) ORDER BY updated DESC`
    const fields = [
      'assignee',
      'issuetype',
      'priority',
      'reporter',
      'status',
      'summary',
    ]

    const jira = this.api('Jira', account.Instance)
    const { issues } = await jira.getIssues({
      jql,
      fields,
    })

    return issues
  }
}
