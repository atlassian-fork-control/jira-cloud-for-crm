const Service = require('../../Service')

module.exports = class extends Service {
  validate (params) {
    const rules = {
      project: { nested_object: {
        id: ['required'],
      } },
    }

    return this.validator(params, rules)
  }

  async execute ({ project }) {
    await this.model.Connection.destroy({
      where: {
        instanceId: this.context('instance.id'),
        projectId: project.id.toString(),
      },
      force: true,
    })
  }
}
