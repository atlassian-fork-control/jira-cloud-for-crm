const { get } = require('lodash')
const getCRMClass = require('../../../common/crm')
const Service = require('../../Service')
const validationRules = require('./validationRules')

module.exports = class extends Service {
  validate (params) {
    const rules = {
      issue: ['required', validationRules.issue],
      changelog: validationRules.changelog,
    }

    return this.validator(params, rules)
  }

  async execute ({ issue, changelog }) {
    // Do not update if reporter was not changed
    if (!changelog || !changelog.items.some(i => i.field === 'reporter')) return

    const connection = await this.model.Connection.findOne({
      include: {
        model: this.model.Account,
        required: true,
      },
      where: {
        instanceId: this.context('instance.id'),
        projectId: issue.fields.project.id,
      },
    })

    if (!connection || !connection.Account.isAuthorized) return

    const CRM = getCRMClass(connection.Account.type)
    const crm = new CRM(this.api.bind(this), connection.Account)

    const [match] = await this.model.Match.findOrBuild({
      where: {
        connectionId: connection.id,
        issueId: issue.id,
        isManual: false,
      },
    })

    const reporter = get(issue, 'fields.reporter')

    const jira = this.api('Jira', this.context('instance'))
    const email = reporter && (
      reporter.emailAddress || await jira.getUserEmail(reporter.accountId)
    )

    // If reporter became empty, we remove match
    if (!email) {
      return match.destroy({ force: true })
    }

    try {
      const matchContactId = await crm.findMatchContactId(email)

      if (matchContactId) {
        match.recordType = 'Contact'
        match.recordId = matchContactId
        await match.save()
      } else {
        await match.destroy({ force: true })
      }
    } catch (error) {
      if (error.code !== 'INVALID_CLIENT') {
        this.logger.warn(
          { err: error },
          'Error while getting contact in issue.updated'
        )
      }
    }
  }
}
