const issue = { nested_object: {
  id: ['required'],
  key: ['required'],
  fields: ['required', { nested_object: {
    project: ['required', { nested_object: {
      id: 'string',
    } }],
    reporter: { nested_object: {
      emailAddress: 'string',
      accountId: 'string',
    } },
  } }],
} }

const changelog = { nested_object: {
  items: { list_of: { nested_object: {
    field: 'string',
    from: 'string',
    fromString: 'string',
    to: 'string',
    toString: 'string',
  } } },
} }

module.exports = { issue, changelog }
