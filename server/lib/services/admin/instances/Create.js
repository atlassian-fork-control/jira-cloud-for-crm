const { chunk: makeChunks } = require('lodash')
const Service = require('../../Service')
const { parseJiraURL } = require('../../util')

module.exports = class extends Service {
  validate (params) {
    const rules = {
      instances: ['required', { list_of: ['required', 'string'] }],
    }

    return this.validator(params, rules)
  }

  async execute ({ instances }) {
    const chunks = makeChunks(instances, 20)

    let results = []

    for (const chunk of chunks) {
      const chunkResults = await Promise.all(chunk.map(this._installAddon))

      results = [...results, ...chunkResults]
    }

    return { results }
  }

  async _installAddon (baseUrl) {
    try {
      baseUrl = parseJiraURL(baseUrl)
    } catch (error) {
      return { baseUrl, error }
    }

    const jira = this.api('Jira', { baseUrl })

    try {
      const result = await jira.installAddon()

      return { baseUrl, result }
    } catch (error) {
      return { baseUrl, error, errorMessage: `${error}` }
    }
  }
}
