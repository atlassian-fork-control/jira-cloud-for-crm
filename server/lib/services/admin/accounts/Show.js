const Service = require('../../Service')
const ServiceError = require('../../Error')

const { formatAccount, formatInstance, formatConnection } = require('../utils')

module.exports = class extends Service {
  validate (params) {
    const rules = {
      id: ['required', 'string'],
    }

    return this.validator(params, rules)
  }

  async execute ({ id }) {
    const account = await this.model.Account.findByPk(id, {
      paranoid: false,
    })

    if (!account) {
      throw new ServiceError('Invalid account id', {
        code: 'WRONG_ID',
        fields: ['id'],
      })
    }

    const instances = await this.model.Instance.findAll({
      include: {
        model: this.model.Connection,
        where: { accountId: id },
      },
    })

    const accountData = formatAccount(account)

    accountData.instances = instances.map((instance) => {
      const instanceData = formatInstance(instance)

      instanceData.connections = instance.Connections.map(formatConnection)

      return instanceData
    })

    return accountData
  }
}
