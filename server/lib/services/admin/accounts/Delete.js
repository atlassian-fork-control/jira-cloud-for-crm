const Service = require('../../Service')
const ServiceError = require('../../Error')

module.exports = class extends Service {
  validate (params) {
    const rules = {
      id: ['required', 'string'],
      force: [{ one_of: [true, false] }],
    }

    return this.validator(params, rules)
  }

  async execute ({ id, force = false }) {
    const account = await this.model.Account.findOne({
      where: { id },
      paranoid: false,
    })

    if (!account) {
      throw new ServiceError('Invalid account id', {
        code: 'WRONG_ID',
        fields: ['id'],
      })
    }

    await account.cache().destroy({ force })
  }
}
