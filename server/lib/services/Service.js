const LIVR = require('livr')
const { get } = require('lodash')

const ServiceError = require('./Error')

LIVR.Validator.defaultAutoTrim(true)

const initNet = require('../common/net')

module.exports = class Service {
  constructor ({ logger, context, model }) {
    this.logger = logger
    this.model = model
    this._context = context
  }

  validator (params, rules) {
    const validator = new LIVR.Validator(rules)
    const result = validator.validate(params)

    if (result) {
      return result
    }

    const fields = validator.getErrors()

    this.logger.warn({ fields, err: new Error('Stack trace') }, 'Validation error')

    throw new ServiceError('Validation error', {
      fields,
      code: 'FORMAT_ERROR',
    })
  }

  context (field) {
    if (field) {
      return get(this._context, field)
    }

    return this._context
  }

  api (className, params) {
    const getNetWrapper = initNet(this.model.sequelize.cache, this.logger)

    return getNetWrapper(className, params)
  }

  async run (params) {
    if (!this.validate) {
      return this.execute()
    }

    const validatedParams = await this.validate(params)

    return this.execute(validatedParams)
  }
}
