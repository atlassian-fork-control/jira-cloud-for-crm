const Service = require('../Service')
const gas = require('../../common/gas')

module.exports = class extends Service {
  async execute () {
    gas.send({
      name: 'uninstall',
      cloudId: this.context('instance.baseUrl'),
      user: this.context('jiraAccountId'),
    })

    await this.context('instance')
      .cache()
      .destroy({ force: true })
  }
}
