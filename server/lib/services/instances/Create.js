const Service = require('../Service')
const ServiceError = require('../Error')
const { parseJiraURL } = require('../util')
const gas = require('../../common/gas')

module.exports = class extends Service {
  validate (params) {
    const rules = {
      id: ['required', 'string'],
      sharedSecret: ['required', 'string'],
      oauthClientId: ['required', 'string'],
      serverVersion: ['required', 'string'],
      pluginsVersion: ['required', 'string'],
      baseUrl: ['required', 'string'],
      description: ['string'],
    }

    return this.validator(params, rules)
  }

  async execute (instance) {
    const signedInstanceId = this.context('instance.id')

    if (signedInstanceId && instance.id !== signedInstanceId) {
      this.logger.warn(
        { body: instance.id, token: signedInstanceId },
        'Different ids passed in body and token'
      )

      throw new ServiceError('Invalid instance id', {
        code: 'PERMISSION_DENIED',
        fields: ['id'],
      })
    }

    instance.baseUrl = parseJiraURL(instance.baseUrl)

    const foundInstance = await this.model.Instance.findByPk(instance.id)

    if (foundInstance && signedInstanceId) { // Update allowed only if request was signed
      this.logger.warn(
        { instanceId: signedInstanceId },
        'Addon was reinstalled on instance'
      )
      await foundInstance.cache().update(instance)

      return
    }

    // Create allowed if no such instance.
    // Sometimes request signed (but we can't validate), sometimes not.
    if (!foundInstance) {
      await this.model.Instance.cache().create(instance)

      gas.send({
        name: 'install',
        cloudId: instance.baseUrl,
      })

      return
    }

    this.logger.warn({ instance }, 'Unauthorised instance update request, possible attack! See #ACEJS-10')

    throw new ServiceError('Invalid instance id', {
      code: 'PERMISSION_DENIED',
      fields: ['id'],
    })
  }
}
