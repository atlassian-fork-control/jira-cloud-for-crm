const { isArray, isObject, isString, delay } = require('lodash')
const ServiceError = require('./Error')

function parseJiraURL (string) {
  if (!string) {
    throw new ServiceError(
      'Jira URL not found',
      { code: 'INVALID_JIRA_CLOUD_URL' }
    )
  }

  const url = new URL(string)

  const protocols = [
    'http:',
    'https:',
  ]

  const domain = url.hostname || url.pathname || ''
  const allowedDomains = require('../common/config').get('addon.allowedDomains') // eslint-disable-line global-require

  if (!allowedDomains.some(allowedDomain => domain.endsWith(allowedDomain))) {
    throw new ServiceError(
      'Not a valid Jira Cloud URL',
      { code: 'INVALID_JIRA_CLOUD_URL' }
    )
  }

  if (url.port.length) {
    throw new ServiceError(
      'Passing port is not allowed',
      { code: 'INVALID_JIRA_CLOUD_URL' }
    )
  }

  if (url.protocol && !protocols.some(protocol => protocol === url.protocol)) {
    throw new ServiceError(
      'Protocol can only be http or https',
      { code: 'INVALID_JIRA_CLOUD_URL' }
    )
  }

  return `https://${domain}`
}

const defaultSecretsFields = [
  'secret',
  'token',
  'oauth',
  'publicKey',
  'body',
  'text',
  'attachments',
  'description',
  'customfield_',
  'displayName',
  'authorization',
]

const allowedFields = [
  'error_description',
]

function copyWithoutSecrets (data, secretsFields = defaultSecretsFields) {
  if (isArray(data)) {
    return data.map(element => copyWithoutSecrets(element, secretsFields))
  }

  if (isObject(data)) {
    return Object.entries(data.dataValues || data).reduce((acc, [key, value]) => {
      const isFieldAllowed = allowedFields
        .some(allowedField => key.toLowerCase().includes(allowedField.toLowerCase()))
      const skip = isFieldAllowed
        ? false
        : secretsFields
          .some(secretField => key.toLowerCase().includes(secretField.toLowerCase()))

      acc[key] = skip && isString(value)
        ? '<MASKED>'
        : copyWithoutSecrets(value, secretsFields)

      return acc
    }, {})
  }

  return data
}

function delayPromised (wait) {
  return new Promise(resolve => delay(resolve, wait))
}

async function tryWhile (retry, condition, retryDelay = 100, maxRetryCount) {
  if (!condition) {
    condition = result => !!result
  }

  while (true) {
    const result = await retry()

    if (condition(result)) {
      return result
    }

    if (maxRetryCount && --maxRetryCount === 0) {
      return Promise.reject(Error('Too many retries'))
    }

    await delayPromised(retryDelay)
  }
}

module.exports = {
  parseJiraURL,
  copyWithoutSecrets,
  delayPromised,
  tryWhile,
}
