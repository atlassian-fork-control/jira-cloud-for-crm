module.exports = {
  async up (queryInterface, DataTypes) {
    await queryInterface.createTable('Connections', {
      id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      instanceId: {
        type: DataTypes.STRING,
        allowNull: false,
        references: {
          model: 'Instances',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      accountId: {
        type: DataTypes.INTEGER,
        allowNull: false,
        references: {
          model: 'Accounts',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      projectId: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      options: {
        type: DataTypes.JSONB,
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      deletedAt: {
        allowNull: true,
        type: DataTypes.DATE,
      },
    })

    const fieldsToIndex = [
      'instanceId',
      'accountId',
      'projectId',
      'createdAt',
      'updatedAt',
      'deletedAt',
    ]

    for (const field of fieldsToIndex) {
      await queryInterface.addIndex('Connections', [field])
    }
  },

  async down (queryInterface) {
    await queryInterface.dropTable('Connections')
  },
}
