module.exports = {
  async up (queryInterface, DataTypes) {
    await queryInterface.createTable('Instances', {
      id: {
        type: DataTypes.STRING,
        allowNull: false,
        primaryKey: true,
      },
      baseUrl: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      sharedSecret: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      oauthClientId: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      description: {
        type: DataTypes.STRING,
        allowNull: false,
        defaultValue: '',
      },
      serverVersion: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      pluginsVersion: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      deletedAt: {
        allowNull: true,
        type: DataTypes.DATE,
      },
    })

    const fieldsToIndex = [
      'baseUrl',
      'serverVersion',
      'pluginsVersion',
      'createdAt',
      'updatedAt',
      'deletedAt',
    ]

    for (const field of fieldsToIndex) {
      await queryInterface.addIndex('Instances', [field])
    }
  },

  async down (queryInterface) {
    await queryInterface.dropTable('Instances')
  },
}
