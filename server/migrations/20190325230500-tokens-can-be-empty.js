module.exports = {
  async up (queryInterface, DataTypes) {
    await queryInterface.sequelize.transaction(async (transaction) => {
      await queryInterface.changeColumn('Accounts', 'accessToken', {
        type: DataTypes.STRING(3000),
        allowNull: true,
      }, { transaction })

      await queryInterface.changeColumn('Accounts', 'refreshToken', {
        type: DataTypes.STRING(3000),
        allowNull: true,
      }, { transaction })

      await queryInterface.changeColumn('Accounts', 'tokenCreatedAt', {
        type: DataTypes.STRING(3000),
        allowNull: true,
      }, { transaction })
    })
  },

  async down (queryInterface, DataTypes) {
    await queryInterface.sequelize.transaction(async (transaction) => {
      await queryInterface.changeColumn('Accounts', 'accessToken', {
        type: DataTypes.STRING(3000),
        allowNull: false,
      }, { transaction })

      await queryInterface.changeColumn('Accounts', 'refreshToken', {
        type: DataTypes.STRING(3000),
        allowNull: false,
      }, { transaction })

      await queryInterface.changeColumn('Accounts', 'tokenCreatedAt', {
        type: DataTypes.STRING(3000),
        allowNull: false,
      }, { transaction })
    })
  },
}
