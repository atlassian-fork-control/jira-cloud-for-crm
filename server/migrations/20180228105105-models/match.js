module.exports = (sequelize, DataTypes) => {
  const Match = sequelize.define('Match', {
    connectionId: {
      type: DataTypes.INTEGER,
      allowNull: false,
    },
    recordType: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    recordId: {
      type: DataTypes.STRING,
      allowNull: false,
    },
    jiraUserKey: {
      type: DataTypes.STRING,
      allowNull: false,
    },
  }, {
    indexes: [
      { fields: ['connectionId'] },
      { fields: ['jiraUserKey'] },
      { fields: ['createdAt'] },
    ],
  })

  Match.associate = function associate (models) {
    this.belongsTo(models.Connection, { foreignKey: 'connectionId' })
  }

  return Match
}
