const initModel = require('./20180228105105-models')
const logger = require('../lib/common/logger').child({ module: 'migration' })
const { sendMessage } = require('../lib/common/sqs/client')

const model = initModel({ logger })

module.exports = {
  async up (queryInterface, DataTypes) {
    const connections = await model.Connection.findAll({
      include: [{
        model: model.Instance,
        require: true,
      }, {
        model: model.Account,
        require: true,
      }, {
        model: model.Match,
      }],
      order: [
        [{ model: model.Match }, 'updatedAt', 'DESC'],
      ],
    })

    await queryInterface.sequelize.transaction(async (t) => {
      const options = { transaction: t }

      await queryInterface.sequelize.query('truncate table "Matches"', options)

      await queryInterface.removeColumn('Matches', 'jiraUserKey', options)

      await queryInterface.addColumn('Matches', 'issueId', {
        type: DataTypes.STRING,
        allowNull: false,
      }, options)

      await queryInterface.addColumn('Matches', 'isManual', {
        type: DataTypes.BOOLEAN,
        allowNull: false,
        defaultValue: false,
      }, options)

      await queryInterface.addIndex('Matches', {
        fields: ['issueId'],
        ...options,
      })
      await queryInterface.addIndex('Matches', {
        fields: ['connectionId', 'issueId', 'recordType', 'recordId'],
        unique: true,
        ...options,
      })
    })

    for (const connection of connections) {
      const [match] = connection.Matches

      await sendMessage('GetIssues', {
        data: {
          connectionId: connection.id,
          projectId: connection.projectId,
          match,
        },
        context: { instance: connection.Instance },
      })
    }
  },

  async down (queryInterface, DataTypes) {
    await queryInterface.addColumn('Matches', 'jiraUserKey', {
      type: DataTypes.STRING,
      allowNull: false,
    })

    await queryInterface.removeColumn('Matches', 'issueId')
    await queryInterface.removeColumn('Matches', 'isManual')
  },
}
