module.exports = {
  async up (queryInterface, DataTypes) {
    await queryInterface.createTable('Accounts', {
      id: {
        type: DataTypes.INTEGER,
        allowNull: false,
        primaryKey: true,
        autoIncrement: true,
      },
      instanceId: {
        type: DataTypes.STRING,
        allowNull: false,
        references: {
          model: 'Instances',
          key: 'id',
        },
        onUpdate: 'CASCADE',
        onDelete: 'CASCADE',
      },
      type: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      userKey: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      baseUrl: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      externalId: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      crmUserId: {
        type: DataTypes.STRING,
        allowNull: false,
      },
      crmUserData: {
        type: DataTypes.JSONB,
      },
      accessToken: {
        type: DataTypes.STRING(2000),
        allowNull: false,
      },
      refreshToken: {
        type: DataTypes.STRING(2000),
        allowNull: false,
      },
      tokenCreatedAt: {
        type: DataTypes.DATE,
        allowNull: false,
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE,
      },
      deletedAt: {
        allowNull: true,
        type: DataTypes.DATE,
      },
    })

    const fieldsToIndex = [
      'instanceId',
      'userKey',
      'type',
      'baseUrl',
      'externalId',
      'crmUserId',
      'createdAt',
      'updatedAt',
      'deletedAt',
    ]

    for (const field of fieldsToIndex) {
      await queryInterface.addIndex('Accounts', [field])
    }
  },

  async down (queryInterface) {
    await queryInterface.dropTable('Accounts')
  },
}
