const { loadFixtures } = require('sequelize-fixtures')
const { Context, fixtures, mocks, utils } = require('../helpers')

const context = new Context()

beforeAll(() => context.begin())
afterAll(() => context.end())

beforeAll(() => loadFixtures(
  [...fixtures.instance, ...fixtures.accounts, ...fixtures.connections],
  context.model
))

describe('Connections.Delete', () => {
  test.skip('Delete connection without admin permission', async () => {
    mocks.jira.listeners.getBearerToken()
    mocks.jira.listeners.getProjectPermissions({
      permissions: {
        ADMINISTER_PROJECTS: { havePermission: false },
        PROJECT_ADMIN: { havePermission: false },
      },
    })

    const { id } = fixtures.connections[0].data

    const authToken = utils.auth.jira({
      jiraAccountId: 'not-admin',
    })

    const { body } = await context.client
      .delete(`/api/connections/${id}`)
      .set('Authorization', authToken)
      .expect(400)

    expect(body).toHaveProperty('error')
    expect(body.error.code).toBe('PERMISSION_DENIED')
  })

  test('Delete last connection of account', async () => {
    mocks.jira.listeners.deleteProjectProperty()

    const { id, accountId } = fixtures.connections[0].data

    const { body } = await context.client
      .delete(`/api/connections/${id}`)
      .set('Authorization', utils.auth.jira())
      .expect(200)

    expect(body).toHaveProperty('ok')

    const connection = await context.model.Connection.findByPk(id)
    const account = await context.model.Account.findByPk(accountId)

    expect(connection).toBeNull()
    expect(account).toBeNull()
  })

  test('Delete connection when another account connections exist', async () => {
    const { id, accountId, projectId } = fixtures.connections[1].data

    mocks.jira.listeners.deleteProjectProperty(projectId)

    await context.client
      .delete(`/api/connections/${id}`)
      .set('Authorization', utils.auth.jira())
      .expect(200)

    const connection = await context.model.Connection.findByPk(id)
    const account = await context.model.Account.findByPk(accountId)

    expect(connection).toBeNull()
    expect(account).toBeTruthy()
  })

  test('No connection was found', async () => {
    const { body } = await context.client
      .delete('/api/connections/111')
      .set('Authorization', utils.auth.jira())
      .expect(400)

    expect(body.error).toEqual({
      message: 'Wrong connection id',
      code: 'WRONG_ID',
      fields: ['id'],
    })
  })
})
