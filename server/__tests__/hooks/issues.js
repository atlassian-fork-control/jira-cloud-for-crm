const { loadFixtures } = require('sequelize-fixtures')
const { Context, fixtures, mocks, utils } = require('../helpers')

const context = new Context()

beforeAll(() => context.begin())
afterAll(() => context.end())

beforeAll(() => loadFixtures(
  [...fixtures.instance, ...fixtures.accounts, ...fixtures.connections],
  context.model
))

describe('Hooks.Issues', () => {
  test('Create new match on issue:created webhook', async () => {
    mocks.salesforce.listeners.getContactByEmail()

    const issue = mocks.jira.hooks.issueCreate

    await context.client
      .post('/api/jira/issues/create')
      .send(issue)
      .set('Authorization', utils.auth.jira())
      .expect(201)

    const match = await context.model.Match.findOne({
      where: {
        connectionId: 1,
        issueId: issue.issue.id,
      },
    })

    expect(match.recordType).toBe('Contact')
    expect(match.recordId).toBe('CONTACTID0')
    expect(match.isManual).toBe(false)
  })

  test('Create new match on issue:updated webhook', async () => {
    mocks.salesforce.listeners.getContactByEmail()

    const issue = mocks.jira.hooks.issueReporterUpdate

    await context.client
      .post('/api/jira/issues/update')
      .send(issue)
      .set('Authorization', utils.auth.jira())
      .expect(201)

    const match = await context.model.Match.findOne({
      where: {
        connectionId: 1,
        issueId: issue.issue.id,
      },
    })

    expect(match.recordType).toBe('Contact')
    expect(match.recordId).toBe('CONTACTID0')
    expect(match.isManual).toBe(false)
  })

  test('Do not update match when reporter is not changed', async () => {
    const issue = mocks.jira.hooks.issueReporterUpdate

    issue.issue.id = '1337'
    issue.changelog.items = []

    await context.client
      .post('/api/jira/issues/update')
      .send(issue)
      .set('Authorization', utils.auth.jira())
      .expect(201)

    const match = await context.model.Match.findOne({
      where: {
        connectionId: 1,
        issueId: issue.issue.id,
      },
    })

    expect(match).toBe(null)
  })

  test('Delete match on issue:deleted webhook', async () => {
    const issue = mocks.jira.hooks.issueDelete

    await context.model.Match.create({
      connectionId: 1,
      issueId: issue.issue.id,
      recordId: 'DUMMY',
      recordType: 'DUMMY',
    })

    await context.client
      .post('/api/jira/issues/delete')
      .send(issue)
      .set('Authorization', utils.auth.jira())
      .expect(201)

    const matches = await context.model.Match.findAll({
      where: {
        connectionId: 1,
        issueId: issue.issue.id,
      },
    })

    expect(matches).toHaveLength(0)
  })
})
