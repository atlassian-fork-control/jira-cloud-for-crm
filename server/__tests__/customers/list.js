const { loadFixtures } = require('sequelize-fixtures')
const nock = require('nock')
const { Context, fixtures, mocks, utils } = require('../helpers')

const context = new Context()

beforeAll(() => context.begin())
afterAll(() => context.end())

beforeAll(() => loadFixtures(
  [...fixtures.instance, ...fixtures.accounts, ...fixtures.connections, ...fixtures.matches],
  context.model
))

describe('Customers.List', () => {
  test('Get Salesforce customers', async () => {
    mocks.salesforce.listeners.getContactList()
    mocks.salesforce.listeners.getAccountList()
    mocks.salesforce.listeners.getLeadList()

    const { body } = await context.client
      .get('/api/customers')
      .query({
        projectId: '1000',
        issueId: '1000',
        search: 'd',
      })
      .set('Authorization', utils.auth.jira())
      .expect(200)

    expect(body).toHaveProperty('ok')
    expect(body).toHaveProperty('data')

    const customers = body.data

    expect(customers).toEqual([
      {
        name: 'Atlassian',
        photo: 'https://test1.salesforce.com/services/images/photo/ACCOUNTID01',
        recordId: 'ACCOUNTID01',
        recordType: 'Account',
      },
      {
        account: 'Atlassian',
        email: 'test.contact1@mail.com',
        name: 'David Lynch',
        photo: 'https://test1.salesforce.com/services/images/photo/CONTACTID01',
        recordId: 'CONTACTID01',
        recordType: 'Contact',
      },
      {
        email: 'test.lead1@mail.com',
        name: 'David Monaco',
        photo: 'https://test1.salesforce.com/services/images/photo/LEADID01',
        recordId: 'LEADID01',
        recordType: 'Lead',
      },
      {
        account: 'Google',
        email: 'test.contact2@mail.com',
        name: 'Josh Davis',
        photo: 'https://test1.salesforce.com/services/images/photo/CONTACTID02',
        recordId: 'CONTACTID02',
        recordType: 'Contact',
      },
    ])
  })

  test('Get Salesforce customers if Lead object is disabled', async () => {
    mocks.salesforce.listeners.getContactList()
    mocks.salesforce.listeners.getAccountList()
    mocks.salesforce.listeners.getLeadListNotFound()

    const { body } = await context.client
      .get('/api/customers')
      .query({
        projectId: '1000',
        issueId: '1000',
        search: 'd',
      })
      .set('Authorization', utils.auth.jira())
      .expect(200)

    expect(body).toHaveProperty('ok')
    expect(body).toHaveProperty('data')

    const customers = body.data

    expect(customers).toEqual([
      {
        name: 'Atlassian',
        photo: 'https://test1.salesforce.com/services/images/photo/ACCOUNTID01',
        recordId: 'ACCOUNTID01',
        recordType: 'Account',
      },
      {
        account: 'Atlassian',
        email: 'test.contact1@mail.com',
        name: 'David Lynch',
        photo: 'https://test1.salesforce.com/services/images/photo/CONTACTID01',
        recordId: 'CONTACTID01',
        recordType: 'Contact',
      },
      {
        account: 'Google',
        email: 'test.contact2@mail.com',
        name: 'Josh Davis',
        photo: 'https://test1.salesforce.com/services/images/photo/CONTACTID02',
        recordId: 'CONTACTID02',
        recordType: 'Contact',
      },
    ])
  })

  test('Failed to get Salesforce account information', async () => {
    mocks.salesforce.listeners.getAccountList()
    mocks.salesforce.listeners.getLeadList()

    const { baseUrl } = fixtures.accounts[0].data

    nock(baseUrl)
      .get('/services/data/v40.0/query')
      .query({
        q: 'select Id, Name, Email, PhotoUrl, Account.Name from Contact where FirstName like \'d%\' or LastName like \'d%\' or Email like \'d%\'',
      })
      .reply(400, () => ({
        error: 'invalid_grant',
        error_description: 'inactive user',
      }))

    const { body } = await context.client
      .get('/api/customers')
      .query({
        projectId: '1000',
        issueId: '1000',
        search: 'd',
      })
      .set('Authorization', utils.auth.jira())
      .expect(400)

    expect(body.error.code).toBe('INVALID_CLIENT')
    expect(body.error.message).toBe('CRM account needs authorization')
  })

  test('Get Dynamics customers', async () => {
    mocks.dynamics.listeners.getContactList()
    mocks.dynamics.listeners.getAccountList()

    const { body } = await context.client
      .get('/api/customers')
      .query({
        projectId: '1001',
        issueId: '1000',
        search: 'd',
      })
      .set('Authorization', utils.auth.jira())
      .expect(200)

    expect(body).toHaveProperty('ok')
    expect(body).toHaveProperty('data')

    const customers = body.data

    expect(customers).toEqual([
      {
        account: '',
        email: 'test.account1@mail.com',
        name: 'Atlassian Ltd',
        photo: 'data:image/jpeg;base64,null',
        recordType: 'Account',
      },
      {
        account: '',
        email: 'test.account2@mail.com',
        name: 'Dynamics',
        photo: 'data:image/jpeg;base64,null',
        recordType: 'Account',
      },
      {
        account: '',
        email: 'test.contact1@mail.com',
        name: 'Александр Жданов',
        photo: 'data:image/jpeg;base64,null',
        recordType: 'Contact',
      },
      {
        account: '',
        email: 'test.contact2@mail.com',
        name: 'Роман Зуев',
        photo: 'data:image/jpeg;base64,null',
        recordType: 'Contact',
      },
    ])
  })

  test('Get HubSpot customers', async () => {
    mocks.hubspot.listeners.getContactList()

    const { body } = await context.client
      .get('/api/customers')
      .query({
        projectId: '1002',
        issueId: '1000',
        search: 'd',
      })
      .set('Authorization', utils.auth.jira())
      .expect(200)

    expect(body).toHaveProperty('ok')
    expect(body).toHaveProperty('data')

    const customers = body.data

    expect(customers).toEqual([
      {
        email: 'test@atlassian.com',
        name: 'David Lynch',
        photo: '',
        recordId: '101',
        recordType: 'Contact',
      },
      {
        email: 'doo@atlassian.com',
        name: 'Doo Yohoho',
        photo: '',
        recordId: '102',
        recordType: 'Contact',
      },
    ])
  })

  test('Get customers without already matched for issue', async () => {
    mocks.hubspot.listeners.getContactList()

    await context.model.Match.create({
      id: 999,
      connectionId: 3,
      issueId: '1000',
      recordType: 'Contact',
      recordId: mocks.hubspot.responses.contactList.contacts[0].vid,
    })

    const { body } = await context.client
      .get('/api/customers')
      .query({
        projectId: '1002',
        issueId: '1000',
        search: 'd',
      })
      .set('Authorization', utils.auth.jira())
      .expect(200)

    expect(body).toHaveProperty('ok')
    expect(body).toHaveProperty('data')

    expect(body.data).toEqual([{
      email: 'doo@atlassian.com',
      name: 'Doo Yohoho',
      photo: '',
      recordId: '102',
      recordType: 'Contact',
    }])
  })

  test('No connection', async () => {
    await context.model.Connection.destroy({
      where: { id: 1 },
      force: true,
    })

    const response = await context.client
      .get('/api/customers')
      .query({ projectId: '1000', issueId: '1000' })
      .set('Authorization', utils.auth.jira())
      .expect(400)

    const { error } = response.body

    expect(error).toEqual({
      message: 'No connection for current project',
      code: 'NO_CONNECTION',
    })
  })
})
