const { Context, mocks, utils } = require('../helpers')

const context = new Context()

beforeAll(() => context.begin())
afterAll(() => context.end())

const instance = mocks.jira.hooks.instanceCreate

describe('Instances.Create', () => {
  test('Install addon on some instance', async () => {
    const response = await context.client
      .post('/api/instances/create')
      .send(instance)

    expect(response.status).toBe(201)
    expect(response.body).toEqual({ ok: true })

    const foundInstance = await context.model.Instance.cache().findByPk(instance.clientKey)

    const {
      id,
      sharedSecret,
      description,
      baseUrl,
      serverVersion,
      pluginsVersion,
      oauthClientId,
    } = foundInstance.get()

    expect(id).toEqual(instance.clientKey)
    expect(sharedSecret).toEqual(instance.sharedSecret)
    expect(baseUrl).toEqual(instance.baseUrl)
    expect(description).toEqual(instance.description)
    expect(pluginsVersion).toEqual(instance.pluginsVersion)
    expect(serverVersion).toEqual(instance.serverVersion)
    expect(oauthClientId).toEqual(instance.oauthClientId)
  })

  test('Update addon on some instance', async () => {
    const jiraToken = utils.auth.jira()

    const { body } = await context.client
      .post('/api/instances/create')
      .set('Authorization', jiraToken)
      .send(instance)
      .expect(201)

    expect(body).toHaveProperty('ok')
  })

  test('Instance can not be reinstalled without proper token', async () => {
    const response = await context.client
      .post('/api/instances/create')
      .query({ user_key: 'testuser' })
      .send(instance)
      .expect(400)

    expect(response.body.error.code).toBe('PERMISSION_DENIED')
  })

  test('Different id\'s passed in body and token', async () => {
    const differentInstanceId = 'jira:main-jira-in-the-w0rld-different-id'
    const jiraToken = utils.auth.jira()

    const { body } = await context.client
      .post('/api/instances/create')
      .set('Authorization', jiraToken)
      .send(Object.assign({}, instance, { clientKey: differentInstanceId }))
      .expect(400)

    expect(body).toHaveProperty('error')
    expect(body.error).toEqual({
      code: 'PERMISSION_DENIED',
      fields: ['id'],
      message: 'Invalid instance id',
    })
  })
})
