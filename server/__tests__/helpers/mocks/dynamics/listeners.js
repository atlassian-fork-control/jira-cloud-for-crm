const nock = require('nock')
const requireDirectory = require('require-directory')

const { responses } = requireDirectory(module)

const baseUrl = responses.accessToken.resource

function getBearerToken () {
  return nock('https://login.microsoftonline.com')
    .post('/common/oauth2/token')
    .query(true)
    .reply(200, responses.accessToken)
}

function whoAmI () {
  return nock(baseUrl)
    .get('/api/data/v8.2/WhoAmI()')
    .reply(200, responses.whoAmI)
}

function getMyself () {
  return nock(baseUrl)
    .get('/api/data/v8.2/systemusers(CRM-DYNAMICS-USER-0)')
    .query({
      $select: 'fullname,internalemailaddress',
    })
    .reply(200, responses.getMyself)
}

function getContactEntityDefinition () {
  return nock(baseUrl)
    .get('/api/data/v8.2/EntityDefinitions')
    .query({
      $select: 'SchemaName',
      $expand: 'Attributes($select=AttributeType,LogicalName,DisplayName)',
      $filter: `SchemaName eq 'Contact'`,
    })
    .reply(200, responses.contactEntityDefinition)
}

function getAccountEntityDefinition () {
  return nock(baseUrl)
    .get('/api/data/v8.2/EntityDefinitions')
    .query({
      $select: 'SchemaName',
      $expand: 'Attributes($select=AttributeType,LogicalName,DisplayName)',
      $filter: `SchemaName eq 'Account'`,
    })
    .reply(200, responses.accountEntityDefinition)
}

function getContactByEmail () {
  return nock(baseUrl)
    .get('/api/data/v8.2/contacts')
    .query({
      $select: 'emailaddress1',
      $filter: `emailaddress1 eq 'test@mail.com'`,
    })
    .reply(200, responses.contact)
}

function getContactById () {
  return nock(baseUrl)
    .get('/api/data/v8.2/contacts')
    .query({
      $select: 'fullname,_parentcustomerid_value,emailaddress1',
      $filter: 'contactid eq CONTACTID0',
    })
    .reply(200, responses.contact)
}

function getAccountById () {
  return nock(baseUrl)
    .get('/api/data/v8.2/accounts')
    .query({
      $select: 'name,emailaddress1',
      $filter: 'accountid eq ACCOUNTID0',
    })
    .reply(200, responses.account)
}

function getContactList () {
  return nock(baseUrl)
    .get('/api/data/v8.2/contacts')
    .query({
      $select: 'fullname,emailaddress1,entityimage',
      $filter: 'contains(fullname,\'d\')',
    })
    .reply(200, responses.contactList)
}

function getAccountList () {
  return nock(baseUrl)
    .get('/api/data/v8.2/accounts')
    .query({
      $select: 'name,emailaddress1,entityimage',
      $filter: 'contains(name,\'d\')',
    })
    .reply(200, responses.accountList)
}

function getCasesByContact () {
  return nock(baseUrl)
    .get('/api/data/v8.2/incidents')
    .query({
      $select: 'title,ticketnumber,createdon,statecode,statuscode,prioritycode',
      $filter: '_customerid_value eq CONTACTID0',
    })
    .reply(200, responses.casesByContact)
}

function getOpportunitiesByContact () {
  return nock(baseUrl)
    .get('/api/data/v8.2/opportunities')
    .query({
      $select: 'name,stepname,actualclosedate',
      $filter: '_parentcontactid_value eq CONTACTID0',
    })
    .reply(200, responses.opportunitiesByContact)
}

function getContactsByAccount () {
  return nock(baseUrl)
    .get('/api/data/v8.2/contacts')
    .query({
      $select: 'fullname,emailaddress1',
      $filter: '_parentcustomerid_value eq ACCOUNTID0',
    })
    .reply(200, responses.contactsByAccount)
}

module.exports = {
  getBearerToken,
  whoAmI,
  getMyself,
  getContactEntityDefinition,
  getAccountEntityDefinition,
  getContactByEmail,
  getContactById,
  getAccountById,
  getContactList,
  getAccountList,
  getCasesByContact,
  getOpportunitiesByContact,
  getContactsByAccount,
}
