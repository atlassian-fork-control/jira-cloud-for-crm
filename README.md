# Jira CRM Integration

## Getting started

* Make sure you have the latest Node.js (12.9.1+)
* Download and install [Docker](https://www.docker.com/products/docker-desktop)
* Download and install [ngrok](https://ngrok.com/download) or run `brew cask install ngrok` or `npm i -g ngrok` in your terminal
* In a dedicated terminal window, run `ngrok http 8080`. This will create an HTTP tunnel and give you a new `https` address that looks like `https://2b141a8d.ngrok.io`

## Register your app

### Salesforce

1. Register app in Salesforce

    * [Sign up](https://developer.salesforce.com/signup) your development Salesforce account
    * Add a new connected app in `Settings` -> `Apps` -> `App Manager`
    * Check checkboxes `Enable OAuth Settings` and `Require Secret for Web Server Flow`
    * Fill `callback URL` field with `https://<your-subdomain>.ngrok.io/api/accounts/create/salesforce/callback`
    * Add following scopes to `Selected OAuth Scopes`:
        * `Full access (full)`
        * `Perform requests on your behalf at any time (refresh_token, offline_access)`
    * Check checkboxes `Force.com Canvas` and `Enable as a Canvas Personal App`
    * Fill `Canvas App URL` field with `https://<your-subdomain>.ngrok.io/api/salesforce`
    * Enable Visualforce Page in `Locations`
    * Save
    * Go to `Manage`
        * Ensure you have `Permitted Users: Admin approved users are pre-authorized`.
        * In Profiles section click on `Manage Profiles` and select profiles that should have access to the app or select all of them

2. Get your `Consumer Key` and `Consumer Secret`. Use it in `server/env`
    * `CRM_SALESFORCE_CLIENT_ID=XXXXXXXXXXXXXXXXXX`
    * `CRM_SALESFORCE_CLIENT_SECRET=XXXXXXXXXXXXXXXXXX`
3. Create Visualforce Page
    * Click settings button and got to Developer Console
    * Create Visualforce Page for each of object (jiraContact, jiraAccount, jiraLead)
    * Insert this lines of code and provide appropriate data:

    ```html
      <apex:page standardController="<Contact|Account|Lead>" showHeader="false">
        <style>body { padding: 0 !important; }</style>
        <apex:canvasApp developerName="<Canvas App Name>" width="100%" height="200px" scrolling="auto" />
      </apex:page>
    ```

3. Insert created Visualforce page to Contacts Page Layout
    * Go to `Setup` -> `Objects and Fields` -> `Object Manager` -> `Contact`
    * Go to `Page Layouts` -> `Contact Layout`
    * Open `Visualforce Pages` tab
    * Create Visualforce Page section. Select 1-column layout. Save and reload the page
    * Drag'n'drop `jiraContact` Visualforce page to created section
    * Click `Save`
    * Open Contact page to see created app
    * *Repeat for Account and Lead Page Layout*

### MS Dynamics

1. Sign Up for [Office Development Program](https://dev.office.com/devprogram) and Reedem Free Trial period
2. Sign up for [Microsoft Dynamics Trial](https://trials.dynamics.com/Dynamics365/Signup/sales)
3. Create App
    * Sign in to <https://portal.office.com/>
    * Go to `Admin` -> `Admin centers` tab -> `Azure AD`
    * In Azure open `Azure Active Directory` -> `App Registrations` -> `New app registration`
    * Set redirect url to `https://<your-subdomain>.ngrok.io/api/accounts/create/dynamics/callback`
    * Add `Dynamics CRM Online` to Required Permissions for created app. Select `Access CRM Online as organization users` scope.
    * In properties set `Multi-tenanted` to YES
4. Get your `Client ID` and `Client Secret`.  Use it in `server/env`.

```bash
CRM_DYNAMICS_CLIENT_ID=XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX
CRM_DYNAMICS_CLIENT_SECRET=XXXXXXXXXXXXXXXXXXXXXXXXXXX
```

### HubSpot

1. Sign Up for [HubSpot Development Account](https://app.hubspot.com/signup/developers/join-hubspot)
2. Create App
    * Sign in to <https://app.hubspot.com>
    * Create new Public App
    * In Scopes enable `Basic OAuth functionality`
3. Get your `Client ID` and `Client Secret`.  Use it in `server/env`.

```bash
CRM_HUBSPOT_CLIENT_ID=XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX
CRM_HUBSPOT_CLIENT_SECRET=XXXXXXXX-XXXX-XXXX-XXXX-XXXXXXXXXXXX
```

4. Create Hubspot CRM Extension
    * Go to `CRM extension API` section
    * Create new `Object Type` with name `Issues`
    * Set Data Fetch URI to `https://<your-subdomain>.ngrok.io/api/hubspot/issues`
    * In `Associated HubSpot objects` enable `Contacts`
5. Create Test HubSpot CRM Account
    * Go to `Testing` Tab
    * Click on `Start Testing`
    * Create test account

## Development process

### Running the environment locally

* Run `npm install` in both `client` and `server` project subdirectories
* In `client` folder run `npm start` to start building client assets and watching for changes
* Update `server/.env` with your `ngrok` URL and a suitable add-on key:

```bash
ADDON_BASE_URL=https://<your-subdomain>.ngrok.io
ADDON_KEY=<some-random-id-to-prevent-collisions-with-other-like-addons>
```

* Start docker containers with service dependencies by running `docker-compose up`
* Run the add-on server `npm start` (to run server in debug mode add DEBUG env variable to the command: `DEBUG=1 npm start`)
* Go to [go.atlassian.com/cloud-dev](go.atlassian.com/cloud-dev) to create your development Jira Cloud instance, if you don't have one
* Install the Jira add-on from the descriptor at `https://<your-subdomain>.ngrok.io/api/addon/descriptor` into your Jira Cloud instance. You have to enable development mode in your Jira by clicking `Settings` link in **Settings**->**Apps**->**Manage add-ons**.

These steps will get your add-on up and running. The server will restart automatically if it detects any changes.

> Use [Postico](https://eggerapps.at/postico/) or [TablePlus](https://tableplus.io) to monitor or interact with DB.
> Go to `localhost:4040` to monitor all requests and incoming webhooks in ngrok GUI.

#### Optional

If you want to upload your source maps to Sentry, create a file `client/.env` and add all required values (example in [client/.env.sample](client/.env.sample) file).

### Before you commit/push

1. Run `npm test` in `server` directory to start all the server-side tests. If this fails in error, you know that it'll fail in Pipelines... so fix it before you commit!
2. If everything looks good, go ahead, commit and push.
3. If your branch is complete, create a [pull request](https://bitbucket.org/atlassian/jira-cloud-for-crm/new) and assign the appropriate reviewers. Once approved, you may merge it into `master`.

## Additional information

### Application configuration

All configuration properties required by the application are provided through environment variables:

* on `server` these values are captured and provided to the application modules by the `lib/common/config.js` module, which uses the [convict](https://github.com/mozilla/node-convict) library. `config.js` defines a template for required configuration and validates at start time, if all values are provided.
* `client` using some environment variables during the build process. See [webpack.config.js](client/webpack.config.js). Also, there's [config.js](client/lib/config.js) where you can specify sentry.io project link, LaunchDarkly keys and so on.

### Common types of fields for different CRM

All CRM's have their own types of fields. Some of them we are using on client and server to format data properly. You have to take this into account and override some types if needed.

#### Common types

* `title` – that's the title name of object. It's a hyperlink to CRM
* `related_item` – hyperlink to related item for child object (e.g. Asset for Entitlement or product for Asset)
* `email` – it's a link and has `mailto:`
* `url` – hyperlink (e.g. website of account)
* `percent` – adds `%` sign if number doesn't have one
* `datetime` – prints out the date and time in format `DD/MMM/YY hh:mm A`
* `date` – prints out the date in format `DD/MMM/YY`
* `address` – we format address on server (`<STREET>, <CITY>, <STATE>, <POSTAL_CODE>, <COUNTRY>`)
* `boolean` - *yes* or *no* values
