import accountsStore from './accountsStore'
import customersStore from './customersStore'

export default {
  accountsStore,
  customersStore
}
