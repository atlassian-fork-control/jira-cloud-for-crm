import { observable, computed, action } from 'mobx'

import Addon from '../services/api/Addon'
import { Flag, getProjectConfigUrl, getContext, getUserInfo } from '../services/JiraAP'
import gas from '../services/gas'
import providers from '../providers.json'
import getPreviewData from '../services/getPreviewData'

class CustomersStore {
  @observable customersRegistry = []
  @observable isConnected = true
  @observable needsAuthorization = false
  @observable projectConfigUrl
  @observable isProcessing = true
  @observable isPreview = false
  @observable provider

  @action
  loadPreviewData ({ provider, previewData }) {
    this.customersRegistry = getPreviewData(provider.id, previewData)
    this.provider = provider
    this.isPreview = true
    this.isProcessing = false
  }

  @action
  async loadCustomersInfo () {
    const user = await getUserInfo()
    const { jira: { issue, project } } = await getContext()

    try {
      const response = await Addon.customers.show(issue.id, project.id)

      this.provider = providers.find(crm => crm.id === response.accountType)

      if (!response || !response.customers.length) {
        this.customersRegistry = []
        this.isProcessing = false

        return gas.send([
          {
            page: 'web-panel',
            name: `visit.${this.provider ? this.provider.id : 'no-crm'}`
          },
          {
            page: 'web-panel',
            name: `no-match`
          }
        ])
      }

      this.customersRegistry = response.customers
      this.isProcessing = false

      gas.send([
        {
          page: 'web-panel',
          name: `visit-crm.${this.provider.id}`
        },
        ...response.customers.map(c => ({
          page: 'web-panel',
          name: c.isManual ? 'manual-match' : 'auto-match'
        }))
      ])
    } catch (error) {
      if (['CRM_ACCOUNT_INACTIVE', 'NO_CONNECTION'].includes(error.code)) {
        this.isConnected = false
        this.projectConfigUrl = await getProjectConfigUrl(project.id, project.key, user.key)

        if (error.code === 'CRM_ACCOUNT_INACTIVE') {
          Flag.warning({
            title: 'Jira CRM integration add-on',
            body: 'CRM account was suspended due to inactivity. If you are the administrator of this project, you can connect your CRM again in Project Settings.'
          })
        }
      }

      if (error.code === 'INVALID_CLIENT') {
        this.needsAuthorization = true
        this.projectConfigUrl = await getProjectConfigUrl(project.id, project.key, user.key)
      }

      this.isProcessing = false

      gas.send({
        page: 'web-panel',
        name: `visit.no-config`
      })
    }
  }

  @computed
  get customersInfo () {
    return this.customersRegistry.reduce((results, m) => {
      const { matchId, values: [match, account], isManual = true } = m
      const newMatch = { ...match, matchId, isManual }

      // Current match has related account
      if (account && (account.id === 'Account' || account.id === 'Company')) {
        const foundIndex = results
        .findIndex(r => r.account && r.account.info.recordId === account.info.recordId)

        // This account is already in array as a parent
        if (foundIndex > -1) {
          results[foundIndex].related = [...results[foundIndex].related, newMatch]
          return results
        }

        return [...results, {
          account,
          related: [newMatch]
        }]
      }

      // Match doesn't has related account so add this object as it is
      return [...results, newMatch]
    }, [])
    .filter((match, i, array) => {
      // In case if this account has been lifted as related account of contact/lead
      // and also it was linked itself,
      // we filter it out. If we'll remove contact with this account,
      // account will show up as a separate link
      if (!match.account && match.id === 'Account') {
        const accountId = match.info.recordId
        return !array.some(m => m.account && m.account.info.recordId === accountId)
      }

      return true
    })
  }

  @action
  async loadChildObjectInfo ({ recordId, recordType, childRecordType }) {
    try {
      const { jira: { issue, project } } = await getContext()
      const response = await Addon.customers.show(issue.id, project.id, recordId, recordType, childRecordType)
      return response.length && response[0]
    } catch (error) {
      if (error.code === 'OBJECT_DISABLED') {
        return { error: 'Entitlement management is disabled in your organization' }
      }

      // This code is unreachable. At least we haven't seen any cases
      return { error: 'Unknown error.' }
    }
  }

  @action
  linkCustomer = async matchCustomer => {
    const { jira: { issue, project } } = await getContext()
    const { recordType, recordId } = matchCustomer
    this.isProcessing = true

    await Addon.match.create({
      recordId,
      recordType,
      projectId: project.id,
      issueId: issue.id
    })

    await this.loadCustomersInfo()
  }

  @action
  unlinkCustomer = async (matchId) => {
    this.isProcessing = true

    const { jira: { project } } = await getContext()

    await Addon.match.delete({
      projectId: project.id,
      matchId
    })

    await this.loadCustomersInfo()
  }
}

export default new CustomersStore()
