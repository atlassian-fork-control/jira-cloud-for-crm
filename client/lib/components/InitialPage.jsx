import React from 'react'
import Button from '@atlaskit/button'
import Spinner from '@atlaskit/spinner'
import { observer } from 'mobx-react'
import { observable } from 'mobx'
import PropTypes from 'prop-types'
import sortBy from 'lodash/sortBy'
import { Dialog } from '../services/JiraAP'

import integrationIllustration from '../../static/img/integrating_product_with_jira.svg'

@observer
export default class InitialPage extends React.Component {
  static propTypes = {
    connectAccount: PropTypes.func.isRequired,
    providers: PropTypes.object.isRequired,
    connecting: PropTypes.bool.isRequired
  }

  @observable selected = ''

  render () {
    const { providers, connectAccount, connecting } = this.props
    const spinner = connecting ? <Spinner /> : null
    const sortedProviders = sortBy(providers, ['name'])

    return (
      <div>
        <img className='integrate-jira-svg' src={integrationIllustration} alt='' />
        <p className='integration-description'>The Jira CRM integration lets you view information about your clients within your Jira issues. Choose a provider below, log in to authenticate your account and select what you want to see.</p>
        <div className='crm-selector'>
          <h4>CRM service</h4>
          <div className='crm-list'>
            {sortedProviders.map(crm => (
              <div
                key={crm.id}
                className={'crm' + (crm.disabled ? ' disabled' : '')}
                onClick={() => (!crm.disabled ? (this.selected = crm.id) : null)}
              >
                <div
                  className={'logo' + (this.selected === crm.id ? ' active' : '')}
                  style={{ backgroundImage: `url(${crm.logo})` }}
                />
                {crm.disabled
                  ? <p className='coming-soon'>Coming soon</p>
                  : null
                }
                <p>{crm.name}</p>
              </div>
            ))}
          </div>
        </div>
        <div className='centered-btn'>
          <Button
            appearance='link'
            onClick={() => Dialog.feedback.show({
              type: 'anotherCRM',
              page: 'project-config'
            })}
          >
            Using another CRM?
          </Button>
        </div>
        <div className='centered-btn'>
          <Button
            appearance='primary'
            onClick={() => connectAccount(this.selected)}
            isDisabled={!this.selected || connecting}
            iconAfter={spinner}
          >
            Continue
          </Button>
        </div>
      </div>
    )
  }
}
