import React, { Component } from 'react'
import { shape, string, number } from 'prop-types'

import Button from '@atlaskit/button'
import Tooltip from '@atlaskit/tooltip'
import Spinner from '@atlaskit/spinner'
import BitbucketPipelinesIcon from '@atlaskit/icon/glyph/bitbucket/pipelines'

import Addon from '../services/api/Addon'

const StatItem = ({ title, number }) => (
  <Tooltip content={title} position='top'>
    <span><em>{title.charAt(0)}:</em> {number}</span>
  </Tooltip>
)

StatItem.propTypes = {
  title: string,
  number: number
}

export default class Sync extends Component {
  static propTypes = {
    id: number,
    stats: shape({
      total: number,
      checked: number,
      matched: number
    })
  }

  constructor (props) {
    super(props)

    const { total, checked, matched } = props.stats

    this.state = {
      status: total === checked ? 'synchronized' : 'in progress',
      total,
      checked,
      matched,
      isLoading: false
    }
  }

  handleClick = async () => {
    this.setState({ isLoading: true })

    await Addon.connection.sync(this.props.id)

    this.setState({
      total: 0,
      checked: 0,
      matched: 0,
      status: 'started',
      isLoading: false
    })
  }

  render () {
    const { status, total, checked, matched, isLoading } = this.state

    return (
      <div className='sync'>
        <div className='stats'>
          <Tooltip content='Reload the page to update' position='top'>
            <div><em>Status:</em> {status}</div>
          </Tooltip>
          <StatItem title='Total issues' number={total} />
          <StatItem title='Checked issues' number={checked} />
          <StatItem title='Matched issues' number={matched} />
        </div>
        <Button
          onClick={this.handleClick}
          iconBefore={isLoading ? <Spinner /> : <BitbucketPipelinesIcon label='Synchronize' />}
        >Sync</Button>
      </div>
    )
  }
}
