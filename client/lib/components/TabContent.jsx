import React from 'react'
import { observable } from 'mobx'
import { observer, inject } from 'mobx-react'
import { object } from 'prop-types'
import sortBy from 'lodash/sortBy'

import { FieldTextStateless } from '@atlaskit/field-text'
import Button from '@atlaskit/button'

import '@atlaskit/reduced-ui-pack'

@inject('state') @observer
export default class TabContent extends React.Component {
  static propTypes = {
    state: object.isRequired,
    group: object.isRequired
  }

  @observable collapsed = true
  @observable searchValue = ''

  updateOptions (e, item, type) {
    const { currentOptions, group } = this.props.state
    let currentGroup = currentOptions.find(group => this.props.group.id === group.id)

    if (!currentGroup) {
      currentGroup = {
        id: group.id,
        label: group.label,
        type: group.type,
        fields: type === 'fields' ? [item] : group.fields,
        objects: type === 'fields' ? group.objects : [item]
      }

      currentOptions.push(currentGroup)
    } else {
      if (e.target.checked) {
        currentGroup[type] = sortBy([...currentGroup[type], item], 'label')
      } else {
        currentGroup[type] = currentGroup[type].filter(field => field.id !== item.id)
      }
    }
  }

  _filterFields (field) {
    const fieldLabel = field.toLowerCase()
    const search = this.searchValue.toLowerCase()

    return fieldLabel.includes(search)
  }

  render () {
    let fields = null
    let objects = null
    let viewMore = null
    const { currentOptions, account } = this.props.state

    if (currentOptions.length) {
      const selected = currentOptions.find(group => group.id === this.props.group.id)
      const group = account.fields.find(group => group.id === this.props.group.id)
      const filteredFields = group.fields.filter(field => this._filterFields(field.label))
      const sortedFields = sortBy(filteredFields, 'label')
      const visibleFields = this.collapsed
        ? sortedFields.slice(0, 12)
        : [...sortedFields]

      fields = (
        <div className='checkboxes'>
          <h4>Fields</h4>
          <div className='filter'>
            <FieldTextStateless
              label=''
              placeholder='Filter fields'
              onChange={e => (this.searchValue = e.target.value)}
              value={this.searchValue}
            />
          </div>
          <ul>{
            visibleFields.length
              ? visibleFields.map((field, index) => {
                const checked = selected.fields.find(selectedField => {
                  return field.id === selectedField.id
                })
                const uniqueKey = group.id + field.id

                return <li key={uniqueKey}>
                  <div className='ak-field-checkbox'>
                    <input
                      type='checkbox'
                      id={uniqueKey}
                      checked={!!checked}
                      onChange={(e) => this.updateOptions(e, field, 'fields')}
                    />
                    <label htmlFor={uniqueKey}>{field.label}</label>
                  </div>
                </li>
              })
              : <li className='empty'>No fields were found</li>
          }</ul>
        </div>
      )

      objects = !!group.objects.length && (
        <div className='checkboxes'>
          <h4>Objects</h4>
          <ul>{
            group.objects.map((object, index) => {
              const checked = selected.objects.find(selectedObject => {
                return object.id === selectedObject.id
              })
              const uniqueKey = group.id + object.id

              return <li key={uniqueKey}>
                <div className='ak-field-checkbox'>
                  <input
                    type='checkbox'
                    id={uniqueKey}
                    checked={!!checked}
                    onChange={(e) => this.updateOptions(e, object, 'objects')}
                  />
                  <label htmlFor={uniqueKey}>{object.label}</label>
                </div>
              </li>
            })
          }</ul>
        </div>
      )

      viewMore = sortedFields.length > 12 && <Button
        appearance='link'
        onClick={() => (this.collapsed = !this.collapsed)}>
        View {this.collapsed ? 'more' : 'less'}
      </Button>
    }

    return (
      <div className='tab-content'>
        <div className='limiting-block'>
          {fields}
          {objects}
        </div>
        {viewMore}
      </div>
    )
  }
}
