import React from 'react'
import AKTabs from '@atlaskit/tabs'
import TabContent from './TabContent'
import { observer } from 'mobx-react'
import { observable } from 'mobx'
import PropTypes from 'prop-types'

@observer
export default class Tabs extends React.Component {
  static propTypes = {
    schema: PropTypes.object.isRequired
  }

  @observable tabState = {
    selectedTab: 0,
    tabs: this.props.schema.map(group => ({
      label: group.label,
      content: <TabContent group={group} />
    }))
  }

  tabSelectHandler = (selectedTabIndex) => {
    this.tabState.selectedTab = selectedTabIndex
  }

  getTabs = () => this.tabState.tabs.map((tab, index) => ({
    ...tab,
    isSelected: index === this.tabState.selectedTab,
    onKeyboardNav: () => null,
    onSelect: () => this.tabSelectHandler(index)
  }))

  render () {
    return (
      <AKTabs
        tabs={this.getTabs()}
        onKeyboardNav={() => null}
      />
    )
  }
}
