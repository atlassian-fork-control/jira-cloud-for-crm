import React from 'react'
import PropTypes from 'prop-types'
import Button from '@atlaskit/button'
import SectionMessage from '@atlaskit/section-message'

export default function ReauthorizePrompt ({ projectConfigUrl }) {
  return (
    <SectionMessage
      appearance='warning'
      linkComponent={({ href }) => (
        <Button
          appearance='link'
          spacing='none'
          href={href}
          target='_blank'
        >Reauthorize</Button>
      )}
      actions={[{ href: projectConfigUrl, key: 'reauthorize' }]}
    >
      The user's account connected to this project is inactive in the CRM or authorization with the CRM has expired. Please ask a project administrator to reauthorize the connection.
    </SectionMessage>
  )
}

ReauthorizePrompt.propTypes = {
  projectConfigUrl: PropTypes.string.isRequired
}
