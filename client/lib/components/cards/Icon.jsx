import React from 'react'
import Tooltip from '@atlaskit/tooltip'
import { string } from 'prop-types'
import { HeaderIcon } from './styled/Card'

Icon.propTypes = {
  content: string,
  src: string.isRequired
}

export default function Icon ({ content, src }) {
  return (
    <Tooltip content={content} position='right' className='test'>
      <HeaderIcon src={src} draggable={false} />
    </Tooltip>
  )
}
