import React from 'react'

export default function ChildContent ({ data, recordType, formatValue }) {
  const { error, id, url, fields, values, label } = data

  if (error) return <p className='empty-object'>{error}</p>

  if (!values.length) {
    return <p className='empty-object'>{`No ${label.toLowerCase()} were found`}</p>
  }

  return values.map((items, i) => (
    <div className='item' key={recordType + id + i}>
      {items.map((value, j) => {
        const uniqKey = recordType + id + fields[j].id + j
        const type = fields[j].type
        const link = url[i]

        value = formatValue(type, value, link)

        return type === 'title'
          ? <div className='title' key={uniqKey}>{value}</div>
          : <div className='field' key={uniqKey}>
            <span className='field-label'>{fields[j].label}:</span>
            <span className='field-value'>{value}</span>
          </div>
      })}
    </div>
  ))
}
