import React, { Component } from 'react'
import { func, string, bool } from 'prop-types'
import { PropTypes as MobXPT } from 'mobx-react'
import gas from '../../services/gas'
import Child from './Child'

export default class ChildrenContainer extends Component {
  static propTypes = {
    objects: MobXPT.observableArray.isRequired,
    recordId: string.isRequired,
    recordType: string.isRequired,
    loadChildObjectInfo: func.isRequired,
    formatValue: func.isRequired,
    isPreview: bool,
    provider: string.isRequired
  }

  state = {
    activeChildObject: null
  }

  toggleChildObject = objectId => {
    this.setState(prevState => {
      const newValue = prevState.activeChildObject !== objectId
        ? objectId
        : null

      return {
        activeChildObject: newValue
      }
    })

    gas.send({
      page: 'web-panel',
      name: `crm.${this.props.provider}.child.${objectId}`
    })
  }

  render () {
    const {
      objects,
      loadChildObjectInfo,
      formatValue,
      recordId,
      recordType,
      isPreview
    } = this.props

    return objects.map(o => (
      <Child
        data={o}
        isOpen={o.id === this.state.activeChildObject}
        recordId={recordId}
        recordType={recordType}
        toggleChildObject={() => this.toggleChildObject(o.id)}
        loadChildObjectInfo={loadChildObjectInfo}
        formatValue={formatValue}
        isPreview={isPreview}
        key={o.id}
      />
    ))
  }
}
