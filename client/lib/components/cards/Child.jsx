import React, { Component } from 'react'
import { bool, string, func, object } from 'prop-types'

import Spinner from '@atlaskit/spinner'

import ChildContent from './ChildContent'
import { ChildWrapper, Header, TitleWrapper } from './styled/Child'
import Chevron from './Chevron'

export default class Child extends Component {
  static propTypes = {
    isOpen: bool.isRequired,
    data: object.isRequired,
    recordType: string.isRequired,
    formatValue: func.isRequired
  }

  state = {
    isDataFetched: false,
    objectData: null,
    isLoading: false
  }

  toggle = async () => {
    const {
      isPreview,
      data,
      recordId,
      recordType,
      toggleChildObject,
      loadChildObjectInfo
    } = this.props

    if (isPreview) return

    if (!this.state.isDataFetched) {
      this.setState({ isLoading: true })

      const objectData = await loadChildObjectInfo({
        recordId,
        recordType,
        childRecordType: data.id
      })

      this.setState({
        isDataFetched: true,
        objectData,
        isLoading: false
      })
    }

    toggleChildObject()
  }

  render () {
    const { isOpen, data, recordType, formatValue } = this.props
    const { isLoading, objectData } = this.state

    const props = {
      data: objectData,
      recordType,
      formatValue
    }

    return (
      <ChildWrapper onClick={this.toggle}>
        <Header>
          <TitleWrapper>
            <Chevron isOpen={isOpen} />{data.label}{isOpen && ':'}
          </TitleWrapper>
          {isLoading && <Spinner />}
        </Header>
        {isOpen && <ChildContent {...props} />}
      </ChildWrapper>
    )
  }
}
