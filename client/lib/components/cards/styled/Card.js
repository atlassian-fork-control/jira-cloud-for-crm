import styled, { css } from 'styled-components'

import { N20, N300, N800 } from './colors'

const getWrapperStyles = ({ appearance }) => {
  if (appearance === 'nested') {
    return css`
      background-color: ${N20};
      border-radius: 5px;
    `
  }

  return css`
    background-color: #fff;
    border-radius: 3px;
    box-shadow: 0 0 1px 0 rgba(9, 30, 66, 0.31);
  `
}

const getWrapperMargin = ({ type }) => {
  return type === 'sub' ? '4px' : '8px'
}

export const Wrapper = styled.div`
  width: calc(100% - 2px);
  margin-left: auto;
  margin-right: auto;
  box-sizing: border-box;

  border: 1px solid ${N20};
  color: ${N800};
  ${props => getWrapperStyles(props)}

  &:not(:last-child) {
    margin-bottom: ${props => getWrapperMargin(props)};
  }
`

const getHeaderMargin = ({ isOpen, appearance }) => {
  return isOpen && appearance !== 'nested' ? '8px' : 0
}

const getHeaderPadding = ({ appearance }) => {
  if (appearance === 'nested') {
    return css`
      padding-left: 17px;
      padding-right: 17px;
    `
  }

  return css`
    padding-left: 8px;
    padding-right: 8px;
  `
}

export const HeaderWrapper = styled.div`
  display: flex;
  justify-content: space-between;
  height: 40px;
  margin-bottom: ${props => getHeaderMargin(props)};
  ${props => getHeaderPadding(props)}
  border-bottom: ${props => props.isOpen
    ? `1px solid ${N20}`
    : 'none'};
`

export const HeaderContent = styled.div`
  display: flex;
  align-items: center;
`

export const HeaderIcon = styled.img`
  width: auto;
  height: 24px;
  margin-right: 7px;
  vertical-align: middle;
  user-select: none;
  image-rendering: -moz-crisp-edges;
  image-rendering: -o-crisp-edges;
  image-rendering: -webkit-optimize-contrast;
  image-rendering: crisp-edges;
  -ms-interpolation-mode: nearest-neighbor;
`

export const HeaderTitle = styled.span`
  margin-right: 8px;
  font-weight: 500;
  user-select: none;
`

export const HeaderActionsWrapper = styled.div`
  margin-right: 4px;
  padding-left: 5px;
  padding-right: 5px;
`

export const ContentWrapper = styled.div`
  padding: 0 17px 11px;
`

export const RelatedsWrapper = styled.div`
  padding: 0 8px 8px;
`

export const FieldWrapper = styled.div`
  &:not(:last-child) {
    margin-bottom: 8px;
  }
`

export const FieldTitle = styled.div`
  font-size: 12px;
  color: ${N300};
`
