const objectsData = {
  Account: {
    id: 'Account',
    label: 'Account',
    info: {
      recordId: 'record01',
      title: 'Account'
    }
  },
  Company: {
    id: 'Company',
    label: 'Company',
    info: {
      recordId: 'record02',
      title: 'Company'
    }
  },
  Contact: {
    id: 'Contact',
    label: 'Contact',
    info: {
      recordId: 'record03',
      title: 'Contact'
    }
  },
  Lead: {
    id: 'Lead',
    label: 'Lead',
    info: {
      recordId: 'record04',
      title: 'Lead'
    }
  }
}

function _buildSchema (provider) {
  const schema = {
    salesforce: ['Account', 'Contact', 'Lead'],
    dynamics: ['Account', 'Contact'],
    hubspot: ['Company', 'Contact']
  }[provider]

  return schema.reduce((res, key) => {
    return {
      ...res,
      [key]: objectsData[key]
    }
  }, {})
}

export default function (provider, configs) {
  if (!configs.length) return []

  const [Account, Contact, Lead] = Object.entries(_buildSchema(provider)).reduce((filledObjects, [key, value]) => {
    const config = configs.find(c => c.id === key)

    if (config) {
      return [...filledObjects, {
        ...value,
        fields: config.fields,
        objects: config.objects
      }]
    }

    return filledObjects
  }, [])

  const matches = [{
    isManual: true,
    matchId: 1,
    values: [Contact, Account]
  }]

  if (Lead) {
    return [...matches, {
      isManual: true,
      matchId: 2,
      values: [Lead, Account]
    }]
  }

  return matches
}
