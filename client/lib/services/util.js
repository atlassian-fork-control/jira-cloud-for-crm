import { User, getLocation } from './JiraAP'

async function getUserAndLocation () {
  let cachedData

  return (async () => {
    if (cachedData && cachedData.user && cachedData.cloudId) {
      return cachedData
    }

    const currentLink = await getLocation()
    const { hostname } = new window.URL(currentLink)
    const cloudId = `https://${hostname}`

    const res = await User.info()
    const user = res.atlassianAccountId

    cachedData = { user, cloudId }

    return cachedData
  })()
}

function parseJSON (response) {
  const contentType = response.headers.get('content-type')

  if (contentType && contentType.includes('application/json')) {
    return response.json()
  }
}

function processResponseBody (responseBody) {
  if (responseBody && responseBody.error) {
    throw responseBody.error
  }

  return responseBody.data
}

function fetch (url, params) {
  return window.fetch(url, params)
    .then(parseJSON)
    .then(processResponseBody)
}

export default {
  fetch,
  getUserAndLocation
}
