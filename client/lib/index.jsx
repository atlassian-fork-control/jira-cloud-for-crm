/* eslint react/prop-types: 0 */

import React from 'react'
import { render } from 'react-dom'
import Loadable from 'react-loadable'
import { useStrict } from 'mobx'
import { Provider } from 'mobx-react'
import { enableLogging } from 'mobx-logger'
import config from './config'

import Page, { Grid, GridColumn } from '@atlaskit/page'
import Spinner from '@atlaskit/spinner'
import ErrorBoundary from './ErrorBoundary'
import NotFound from './apps/NotFound'

import '@atlaskit/css-reset'
import './styles/app.scss'

if (config.environment === 'development') enableLogging()
useStrict()

const routes = {
  '/project_config': { component: 'Jira/ProjectConfig' },
  '/issue_panel': { component: 'Jira/IssuePanel' },
  '/post_install': { component: 'Jira/PostInstall' },
  '/match': { component: 'Jira/MatchDialog' },
  '/feedback': { component: 'Jira/FeedbackDialog' },
  '/account_connected': { component: 'Jira/AccountConnected', noAP: true },
  '/error': { component: 'Jira/ErrorPage', noAP: true },
  '/404': { component: 'NotFound', noAP: true },

  '/salesforce_issues_list': { component: 'Salesforce/App', noAP: true },
  '/dynamics_pre_auth': { component: 'Dynamics/PreAuthPage', noAP: true }

}

const DynamicComponent = Loadable({
  loading: ({ error }) => error ? <NotFound /> : <Spinner />,
  async loader () {
    const route = routes[window.location.pathname] || routes['/404']

    if (!(window.AP || route.noAP)) {
      route.component = 'FailedToLoad'
    }

    const componentModule = await import(`./apps/${route.component}`)
    const Component = componentModule.default

    if (Component.isMobxInjector) {
      // Load stores only for Components that have @inject in declaration
      const storesModule = await import('./stores')

      return { Component, stores: storesModule.default }
    }

    return { Component }
  },

  render (loaded) {
    const { Component, stores } = loaded

    if (stores) {
      return <Provider {...stores}><Component /></Provider>
    }

    return <Component />
  }
})

render(
  <ErrorBoundary>
    <Page>
      <Grid>
        <GridColumn>
          <DynamicComponent />
        </GridColumn>
      </Grid>
    </Page>
  </ErrorBoundary>,
  document.getElementById('app')
)
