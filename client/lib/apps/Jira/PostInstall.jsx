import React from 'react'
import { observable, computed, action } from 'mobx'
import { observer } from 'mobx-react'

import { StatelessSelect } from '@atlaskit/single-select'
import Avatar from '@atlaskit/avatar'
import Button from '@atlaskit/button'

import logo from '../../../static/img/crmforjira.svg'
import { getProjectConfigUrl } from '../../services/JiraAP'
import { getProjects } from '../../services/api/Jira'

@observer
export default class GetStarted extends React.Component {
  @observable state = {
    projects: [],
    projectConfigUrl: '',
    select: {
      isLoading: true,
      isSelectOpen: false,
      filterValue: '',
      selectedItem: {}
    }
  }

  @computed
  get items () {
    return [
      {
        items: this.state.projects.map(project => ({
          content: project.name,
          value: project.id,
          elemBefore: <Avatar src={project.avatarUrls['48x48']} />
        }))
      }
    ]
  }

  @action
  toggleOpen = attr => {
    this.state.select.isSelectOpen = attr.isOpen
  }

  @action
  handleSelected = async item => {
    this.state.select = {
      isSelectOpen: false,
      selectedItem: item,
      filterValue: ''
    }

    const project = this.state.projects.find(project => item.value === project.id)
    const projectConfigUrl = await getProjectConfigUrl(project)
    this.state.projectConfigUrl = projectConfigUrl
  }

  @action
  updateFilter = value => {
    if (!value.length) {
      this.state.select.selectedItem = {}
    }

    this.state.select.filterValue = value
  }

  async componentDidMount () {
    const projects = await getProjects()

    this.state.projects = projects
    this.state.select.isLoading = false
  }

  render () {
    const docLink = <a
      href='https://confluence.atlassian.com/jiracorecloud/crm-for-jira-cloud-950809846.html'
      target='_blank'
    >our documentation</a>

    return (
      <div className='get-started'>
        <img src={logo} />
        <h3>Thanks for installing CRM Integration for Jira Cloud</h3>
        <div className='project-select'>
          <StatelessSelect
            items={this.items}
            isOpen={this.state.select.isSelectOpen}
            onOpenChange={this.toggleOpen}
            onFilterChange={this.updateFilter}
            filterValue={this.state.select.filterValue}
            onSelected={this.handleSelected}
            selectedItem={this.state.select.selectedItem}
            label='Get started by choosing a project:'
            isLoading={this.state.select.isLoading}
            hasAutocomplete
            shouldFocus
          />
          <br />
          <Button
            appearance='primary'
            className='connect-button'
            isDisabled={!this.state.select.selectedItem.value}
            href={this.state.projectConfigUrl}
            target='_blank'>
            Connect
          </Button>
        </div>
        <p>Visit {docLink} for more information.</p>
      </div>
    )
  }
}
