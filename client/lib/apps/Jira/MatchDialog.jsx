import React from 'react'
import { observable, computed } from 'mobx'
import { inject, observer } from 'mobx-react'

import Button from '@atlaskit/button'
import { DynamicTableStateless } from '@atlaskit/dynamic-table'
import Avatar, { AvatarItem } from '@atlaskit/avatar'
import { FieldTextStateless } from '@atlaskit/field-text'
import Spinner from '@atlaskit/spinner'

import Addon from '../../services/api/Addon'
import { Dialog, getProjectConfigUrl } from '../../services/JiraAP'

import ReauthorizePrompt from '../../components/ReauthorizePrompt'

@inject('customersStore')
@observer
export default class Match extends React.Component {
  @observable projectId
  @observable issueId
  @observable provider
  @observable processing = false
  @observable needsAuthorization = false
  @observable projectConfigUrl
  @observable customers
  @observable searchValue = ''
  @observable selected
  @observable currentPage = 1

  head = {
    cells: [
      {
        key: 'name',
        content: 'Name',
        width: 50
      },
      {
        key: 'email',
        content: 'Email',
        width: 50
      }
    ]
  }

  async componentDidMount () {
    Dialog.submitButton.disable()

    window.AP.dialog.getCustomData(({ projectId, issueId, provider }) => {
      this.projectId = projectId
      this.issueId = issueId
      this.provider = provider
    })

    window.AP.dialog.disableCloseOnSubmit()

    window.AP.events.on('dialog.submit', () => {
      window.AP.dialog.close(this.selected)
    })
  }

  @computed get rows () {
    return this.customers.map((customer, i) => {
      const icon = this.provider && <img
        className='icon'
        size='xxlarge'
        src={this.provider.icons[customer.recordType.toLowerCase()]}
      />

      return ({
        className: this.selected === customer ? 'selected' : '',
        onClick: () => this.selectRow(customer),
        cells: [
          {
            key: i,
            content: (
              <div className='cell'>
                <AvatarItem
                  avatar={<Avatar src={customer.photo} presence={icon} />}
                  key={customer.name + i}
                  primaryText={customer.name}
                  secondaryText={customer.account || ''}
                />
              </div>
            )
          },
          {
            key: customer.email + i,
            content: (
              <div className='cell'>
                {customer.email}
              </div>
            )
          }
        ]
      })
    })
  }

  searchHandler = async (e) => {
    e.preventDefault()

    this.processing = true
    Dialog.submitButton.disable()

    this.selected = null

    try {
      this.customers = await Addon.customers.list({
        projectId: this.projectId,
        issueId: this.issueId,
        search: this.searchValue
      })
    } catch (error) {
      if (error.code === 'INVALID_CLIENT') {
        this.projectConfigUrl = await getProjectConfigUrl()
        this.needsAuthorization = true
      }
    }

    this.processing = false
  }

  selectRow = (customer) => {
    if (!this.selected) Dialog.submitButton.enable()
    this.selected = customer
  }

  onSetPage = (page) => {
    this.currentPage = page
  }

  render () {
    const spinner = this.processing ? <Spinner /> : null

    if (this.needsAuthorization) {
      return (
        <div className='dialog-window'>
          <ReauthorizePrompt projectConfigUrl={this.projectConfigUrl} />
        </div>
      )
    }

    return (
      <div className='dialog-window'>
        <form className='form-group' onSubmit={e => this.searchHandler(e)}>
          <div className='input'>
            <FieldTextStateless
              label='Customer email or name'
              onChange={e => (this.searchValue = e.target.value)}
              value={this.searchValue}
              autoFocus
              compact
            />
          </div>
          <Button
            appearance='primary'
            className='find-button'
            type='submit'
            isDisabled={this.processing || !this.searchValue.length}
            iconBefore={spinner}>
            Search
          </Button>
        </form>
        {
          this.customers
            ? <DynamicTableStateless
              isFixedSize
              head={this.head}
              rows={this.rows}
              rowsPerPage={5}
              onSetPage={this.onSetPage}
              page={this.currentPage}
              emptyView='No customer found'
            />
            : null
        }
      </div>
    )
  }
}
