import 'url-search-params-polyfill'
import React from 'react'
import { render } from 'react-dom'
import { observer } from 'mobx-react'
import { observable } from 'mobx'
import get from 'lodash/get'
import { Spinner, Alert } from 'react-lightning-design-system'
import IssuesTable from './IssuesTable'

import '@salesforce-ux/design-system/assets/styles/salesforce-lightning-design-system.min.css'
import './style.scss'

const { fetch, URLSearchParams } = window

const supportedObjects = ['Contact', 'Account', 'Lead']

@observer
export default class App extends React.Component {
  @observable issues
  @observable errorMessage
  @observable oauthRequired

  async componentDidMount () {
    const urlParams = new URLSearchParams(window.location.search)
    const oauthRequired = urlParams.get('oauthRequired')

    if (oauthRequired) {
      this.oauthRequired = true
      return
    }

    const jwt = urlParams.get('jwt')
    const recordId = urlParams.get('recordId')
    const recordType = urlParams.get('recordType')

    if (!supportedObjects.includes(recordType)) {
      this.errorMessage = `Only ${supportedObjects.join(', ')} object types are supported`
      return
    }

    const response = await fetch(`/api/salesforce/issues?jwt=${jwt}&recordId=${recordId}&recordType=${recordType}`)
    const body = await response.json()
    this.issues = body.data
  }

  render () {
    if (this.oauthRequired) {
      return <Alert>
        <b>Connect Jira project to Salesforce to see issues here</b>
      </Alert>
    }

    if (this.errorMessage) {
      return <Alert level='warning'>
        <b>{this.errorMessage}</b>
      </Alert>
    }

    if (!this.issues) {
      return <div className='spinner'>
        <Spinner type='brand' size='medium' />
        Loading issues...
      </div>
    }

    const statusDictionary = {
      new: 'new',
      indeterminate: 'inprogress',
      done: 'success'
    }

    const issues = this.issues.map(issue => ({
      link: issue.link,
      key: issue.key,
      summary: issue.summary,
      type: {
        name: get(issue, 'type.name'),
        iconUrl: get(issue, 'type.iconUrl')
      },
      status: {
        name: get(issue, 'status.name'),
        type: statusDictionary[get(issue, 'status.statusCategory.key', 'default')]
      },
      priority: {
        name: get(issue, 'priority.name'),
        iconUrl: get(issue, 'priority.iconUrl')
      },
      reporter: {
        name: get(issue, 'reporter.displayName'),
        photo: get(issue, 'reporter.avatarUrls.32x32'),
        accountId: get(issue, 'reporter.accountId')
      },
      assignee: {
        name: get(issue, 'assignee.displayName'),
        photo: get(issue, 'assignee.avatarUrls.32x32'),
        accountId: get(issue, 'assignee.accountId')
      }
    }))

    return (
      <div>
        <IssuesTable issues={issues} />
      </div>
    )
  }
}

render(
  <App />,
  document.getElementById('app')
)
